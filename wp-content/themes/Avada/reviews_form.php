<?php
   // Template Name: Reviews Form
   get_header(); ?>
   
<div class="review_form main_content">
   	<div class="container">
    	<div class="row">
        	<div class="col-lg-8 offset-lg-2 rev_form">
            	<h1>Write Your Feedback</h1>
                <?php
                    if (!empty($_POST)) {
                        global $wpdb;
                        $table = wp_reviews;
                        $data = array(
                            'name' => $_POST['visitor_name'],
                            'feedback'    => $_POST['visitor_feedback'],
                            'rating'    => $_POST['visitor_rating']
                        );
                        $format = array(
                            '%s',
                            '%s',
                            '%s'
                        );
                        $success=$wpdb->insert( $table, $data, $format );
                        if($success){
                            echo '<h2>Review Saved Successfully</h2>' ; 
                        }
                    } else {
                ?>
                <form method="post" action="<?php echo get_option('siteurl'); ?>/review-submit/">
                	<div class="row">
        				<div class="col-md-6">
                        	<label>City</label>
                            <input type="text" placeholder="Enter Your City">
                        </div>
                        
                        <div class="col-md-6">
                        	<label>Name</label>
                            <input name="visitor_name" type="text" placeholder="Enter Your Name" required>
                        </div>
                  	</div>
                    
                    <div class="row">
        				<div class="col-md-6">
                        	<label>Subject</label>
                            <input type="text" placeholder="Review Title">
                        </div>
                        
                        <div class="col-md-6">
                        	<label>Give Your Rating</label>
                            <select name="visitor_rating">
                            	<option value="1">&#9733;</option>
                                <option value="2">&#9733;&#9733;</option>
                                <option value="3">&#9733;&#9733;&#9733;</option>
                                <option value="4">&#9733;&#9733;&#9733;&#9733;</option>
                                <option value="5">&#9733;&#9733;&#9733;&#9733;&#9733;</option>
                            </select>
                        </div>
                  	</div>
                    
                    <div class="row">
        				<div class="col-md-12">
                        	<label>Write Your Feedback</label>
                            <textarea name="visitor_feedback" placeholder="Please Write Your Feedback...." required></textarea>
                        </div>
                  	</div>
                    
                    <div class="row">
        				<div class="col-md-12">
                        	<input type="submit" value="Submit Your Feedback">
                        </div>
                  	</div>
                </form>
                <?php } ?>
            </div>
       	</div>
   </div>
</div>

<?php get_footer(); ?>