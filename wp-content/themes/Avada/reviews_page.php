<?php
    // Template Name: Reviews
    
    get_header();
    global $wpdb;
    $sql = "SELECT * FROM wp_reviews";
    $userReviews = $wpdb->get_results($sql, OBJECT);
    $averageRating = 0;
    $totalReviews = count($userReviews);
    
    //calculate overall rating
    foreach($userReviews as $re){
        $averageRating = $averageRating + $re->rating;
    }
    
    $finalRating = $averageRating / $totalReviews;
?>
<div class="review_banner">
	<h2>Genuine EssaysnAssignments Reviews</h2>
    <p>We do not invest in making fake claims</p>
    <span>See....98% Customers are happy customers!</span>
    <div class="rt_box">
    	<p><?= number_format($finalRating, 2, '.', ''); ?>/5</p>
        <span>
            <?php 
            
            if($finalRating == 1){ echo '<i class="fa fa-star"></i>'; }
            else if($finalRating > 1 && $finalRating < 2){ echo '<i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>'; }
            else if($finalRating == 2){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>'; }
            else if($finalRating > 2 && $finalRating < 3){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>'; }
            else if($finalRating == 3){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>'; }
            else if($finalRating > 3 && $finalRating < 4){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>'; }
            else if($finalRating == 4){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>'; }
            else if($finalRating > 4 && $finalRating < 5){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>'; }
            else if($finalRating == 5){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>'; }
            
            ?>
        <span>OVERALL RATING</span></span>
    </div>
    
    <div class="rt_box">
    	<p><?= $totalReviews; ?></p>
        <span>USER REVIEWS</span>
    </div>
</div>

<div class="review_content main_content">
   	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<h1>EssaysnAssignments Reviews</h1>
            </div>
       	</div>
      	<div class="row">
			<?php
			    if(count($userReviews)):
                foreach($userReviews as $review){
            ?>

            	<div class="col-lg-6 rev_box">
                	<div class="rev_txt">
                    	<h2>"<?= $review->name; ?>"</h2>
                        <p><?= $review->feedback; ?></p>
                        <span>Pusblished on <?= $review->created_on; ?></span>
                    </div>
                    <div class="rev_user">
                    	<img src="/wp-content/uploads/2020/05/review-person.png" alt="review-user">
                        <p>
                            <?php 
                            
                            if($review->rating == 1){ echo '<i class="fa fa-star"></i>'; }
                            else if($review->rating > 1 && $review->rating < 2){ echo '<i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>'; }
                            else if($review->rating == 2){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>'; }
                            else if($review->rating > 2 && $review->rating < 3){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>'; }
                            else if($review->rating == 3){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>'; }
                            else if($review->rating > 3 && $review->rating < 4){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>'; }
                            else if($review->rating == 4){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>'; }
                            else if($review->rating > 4 && $review->rating < 5){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i>'; }
                            else if($review->rating == 5){ echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>'; }
                            
                            ?>
                        </p>
                    </div>
    			</div>

			<?php

				}
				endif;

			?>

		</div>
   </div>
</div>

<?php get_footer(); ?>