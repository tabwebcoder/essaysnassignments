<?php
/**
 * Plugin Name:       Crm Setting
 * Description:       Crm Setting!
 * Version:           1.0.0
 * Author:            Husnain ali
 * Author URI:        http://husnain.pk
 * Text Domain:       Husnain
 * License:           GPL-2.0+
 */
/*
 * Plugin constants
 */
if (!defined('CrmSetting_URL'))
    define('CrmSetting_URL', plugin_dir_url(__FILE__));
if (!defined('CrmSetting_PATH'))
    define('CrmSetting_PATH', plugin_dir_path(__FILE__));

/*
 * Main class
 */
/**
 * Class CrmSetting
 *
 * This class creates the option page and add the web app script
 */

class CrmSetting {

    /**
     * CrmSetting constructor.
     *
     * The main plugin actions registered for WordPress
     */
    public function __construct() {
        global $wpdb;
// Admin page calls:
         add_action('admin_menu', array($this, 'addAdminMenu'));

    }

    public function addAdminMenu() {
        add_menu_page(__('CrmSetting', 'CrmSetting'), __('CrmSetting', 'CrmSetting'), 'manage_options', 'CrmSetting', array($this, 'adminLayout'), '');

    }

    public function adminLayout() {
      global $wpdb;
             
if (isset($_POST['CrmSetting-admin-save'])) {
  $CrmID = $_POST['CrmID'];
          $crm_name = $_POST['crm_name'];
          $host = $_POST['host'];
          $username = $_POST['username'];
          $password = $_POST['password'];
          $database = $_POST['database'];
          $crm_name  = $_POST['crm_name'];
          $status = $_POST['status'];
            if (empty($CrmID)) {
                $insert_query = "INSERT INTO `{$wpdb->prefix}crm_settings`
                                  set crm_name = '$crm_name',
                                  host_crm = '$host',
                                  username_crm = '$username',
                                  password_crm = '$password',
                                  database_crm = '$database',
                                  status_crm =   '$status'";
                $wpdb->query($insert_query);
            } else if (!empty($CrmID)) {
                $insert_query = "update `{$wpdb->prefix}crm_settings`
                                  set crm_name = '$crm_name',
                                  host_crm = '$host',
                                  username_crm = '$username',
                                  password_crm = '$password',
                                  database_crm = '$database',
                                  status_crm =   '$status'
                                  where ID = $CrmID";
                $wpdb->query($insert_query);
            }

}
          $crm_settings = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}crm_settings` limit 1");
          if( count($crm_settings) > 0){
                               $connection_crm = mysqli_connect($crm_settings[0]->host_crm, $crm_settings[0]->username_crm, $crm_settings[0]->password_crm, $crm_settings[0]->database_crm);
            if (isset($_POST['SmtpSetting-admin-save'])) {

                    $ID = $_POST['ID'];
            $host = $_POST['host'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $SMTPSecure = $_POST['SMTPSecure'];
            $Port = $_POST['Port'];
            $sendfrom = $_POST['sendfrom'];
             $sendto = $_POST['sendto'];
            $Status = $_POST['status'];
            if (empty($ID)) {
                    $sql_smtp_insert = "INSERT INTO `Smtp_settings`
                    set host = '$host',
                    username = '$username',
                    password = '$password',
                    SMTPSecure = '$SMTPSecure',
                    port = '$Port',
                    sendfrom = '$sendfrom',
                    sendto = '$sendto',
                    status= '$Status'";
                $result_smtp_insert = $connection_crm->query($sql_smtp_insert);
                $smtp_insert_id = mysqli_insert_id($connection_crm);
            } else if (!empty($ID)) {
                 $sql_smtp_update = "update `Smtp_settings`
                    set host = '$host',
                    username = '$username',
                    password = '$password',
                    SMTPSecure = '$SMTPSecure',
                    port = '$Port',
                    sendfrom = '$sendfrom',
                    sendto = '$sendto',
                    status= '$Status' where id =$ID ";
                $result_smtp_update = $connection_crm->query($sql_smtp_update);
            }

          }

  $sql_smtp = "select * from Smtp_settings limit 1";
                $result_smtp = $connection_crm->query($sql_smtp);
                $row_smtp = $result_smtp->fetch_assoc();
}

           
        ?>

        <div class="wrap">
          <h3><?php _e('Crm Settings', 'Crm Setting'); ?> Please link with CRM first</h3>

                    <hr>
                    <table class="form-table">
                        <tbody>
                        <form action="?page=CrmSetting" method="post">
                            <input name="CrmID" type="hidden" id="CrmID" class="regular-text"
                                   value="<?php echo (isset($crm_settings[0]->id)) ? $crm_settings[0]->id : ''; ?>"/>
                                   <tr>
                                <td scope="row">
                                    <label><?php _e('Crm Name', 'Crm Nam'); ?></label>
                                </td>
                                <td>
                                    <input name="crm_name"
                                           id="crm_name"
                                           class="regular-text"
                                           value="<?php echo (isset($crm_settings[0]->crm_name)) ? $crm_settings[0]->crm_name : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Host', 'Host'); ?></label>
                                </td>
                                <td>
                                    <input name="host"
                                           id="host"
                                           class="regular-text"
                                           value="<?php echo (isset($crm_settings[0]->host_crm)) ? $crm_settings[0]->host_crm : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Username', 'Username'); ?></label>
                                </td>
                                <td>
                                      <input name="username"
                                           id="username"
                                           class="regular-text"
                                           value="<?php echo (isset($crm_settings[0]->username_crm)) ? $crm_settings[0]->username_crm : ''; ?>"/>
                                </td>
                            </tr>
                              <tr>
                                <td scope="row">
                                    <label><?php _e('Password', 'Password'); ?></label>
                                </td>
                                <td>
                                    <input name="password"
                                           id="password"
                                           class="regular-text"
                                           value="<?php echo (isset($crm_settings[0]->password_crm)) ? $crm_settings[0]->password_crm : ''; ?>"/>
                                </td>
                            </tr>
                           
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Database', 'Database'); ?></label>
                                </td>
                                <td>
                                     <input name="database"
                                           id="database"
                                           class="regular-text"
                                           value="<?php echo (isset($crm_settings[0]->database_crm)) ? $crm_settings[0]->database_crm: ''; ?>"/>
                                </td>
                            </tr>
                            
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Status', 'CrmSetting'); ?></label>
                                </td>
                                <td>

                                    <input type="radio" name="status" id="status"  value="1" <?php echo (isset($crm_settings[0]->status_crm) && $crm_settings[0]->status_crm == 1 ) ? 'checked="checked"' : ''; ?>>
                                    <label>
                                        <for="pp">Active</label>&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                    <input
                                        type="radio" name="status" id="status" value="0"  <?php echo (isset($crm_settings[0]->status_crm) && $crm_settings[0]->status_crm == 0 ) ? 'checked="checked"' : ''; ?>> <label for="status">Deactive
                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <button class="button button-primary" id="CrmSetting-admin-save" name="CrmSetting-admin-save"  type="submit"><?php _e('Save', 'CrmSetting'); ?></button>
                                </td>
                            </tr>
                        </form>
                        </tbody>

                    </table>

                    <hr>
            <h3><?php _e('Smtp Settings', 'Smtp Setting'); ?></h3>

                    <hr>
                    <table class="form-table">
                        <tbody>
                        <form action="?page=CrmSetting" method="post">
                            <input name="ID" type="hidden" id="ID" class="regular-text"
                                   value="<?php echo (isset($row_smtp['id'])) ? $row_smtp['id'] : ''; ?>"/>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Host', 'Host'); ?></label>
                                </td>
                                <td>
                                    <input name="host"
                                           id="host"
                                           class="regular-text"
                                           value="<?php echo (isset($row_smtp['host'])) ? $row_smtp['host'] : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Username', 'Username'); ?></label>
                                </td>
                                <td>
                                      <input name="username"
                                           id="username"
                                           class="regular-text"
                                           value="<?php echo (isset($row_smtp['username'])) ? $row_smtp['username'] : ''; ?>"/>
                                </td>
                            </tr>
                              <tr>
                                <td scope="row">
                                    <label><?php _e('Password', 'Password'); ?></label>
                                </td>
                                <td>
                                    <input name="password"
                                           id="password"
                                           class="regular-text"
                                           value="<?php echo (isset($row_smtp['password'])) ? $row_smtp['password'] : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('SMTPSecure', 'SMTPSecure'); ?></label>
                                </td>
                                <td>
                                    <input name="SMTPSecure"
                                           id="SMTPSecure"
                                           class="regular-text"
                                           value="<?php echo (isset($row_smtp['SMTPSecure'])) ? $row_smtp['SMTPSecure'] : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Port', 'Port'); ?></label>
                                </td>
                                <td>
                                     <input name="Port"
                                           id="Port"
                                           class="regular-text"
                                           value="<?php echo (isset($row_smtp['Port'])) ? $row_smtp['Port'] : ''; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Sending From ', 'Sending From'); ?></label>
                                </td>
                                <td>
                                    <input name="sendfrom"
                                           id="sendfrom"
                                           class="regular-text"
                                           value="<?php echo (isset($row_smtp['sendfrom'])) ? $row_smtp['sendfrom'] : ''; ?>"/>
                                </td>
                            </tr>

                             <tr>
                                <td scope="row">
                                    <label><?php _e('Sending To ', 'Sending To'); ?></label>
                                </td>
                                <td>
                                    <input name="sendto"
                                           id="sendto"
                                           class="regular-text"
                                           value="<?php echo (isset($row_smtp['sendto'])) ? $row_smtp['sendto'] : ''; ?>"/>
                                </td>
                            </tr>
                            
                            <tr>
                                <td scope="row">
                                    <label><?php _e('Status', 'SmtpSetting'); ?></label>
                                </td>
                                <td>

                                    <input type="radio" name="status" id="status"  value="1" <?php echo (isset($row_smtp['status']) && $row_smtp['status'] == 1 ) ? 'checked="checked"' : ''; ?>>
                                    <label>
                                        <for="pp">Active</label>&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                    <input
                                        type="radio" name="status" id="status" value="0"  <?php echo (isset($row_smtp['status']) &&$row_smtp['status'] == 0 ) ? 'checked="checked"' : ''; ?>> <label for="status">Deactive
                                    </label>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <button class="button button-primary" id="SmtpSetting-admin-save" name="SmtpSetting-admin-save"  type="submit"><?php _e('Save', 'SmtpSetting'); ?></button>
                                </td>
                            </tr>
                        </form>
                        </tbody>

                    </table>
                    <hr>
   
                    </div>

                    <?php
                }
            }

            /*
             * Starts our plugin class, easy!
             */
            new CrmSetting();



            