=== Booking and Rental System (Woocommerce) ===
Contributors: redq
Donate link:
Tags:  woocommerce, google calendar, full calendar, daily price, monthly price, weekly price, availability checking, car rental system,agenda, booking, booking system, calendar, car rental, hotel, listings, management system, real estate, rentals, reservation, reservation system, woo-booking
Requires at least: 4.7
Tested up to: 4.7.4
Stable tag: 1.0.4
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Integrate a booking / reservation system into your WordPress website.

== Description ==

Booking and Rental System is a user-friendly booking plugin built with woocommerce. This powerful plugin allows you to sell your time or date based bookings. It creates a new product type to your WooCommerce site. Perfect for those wanting to offer rental , booking , or real state agencies or services.


== Visit Premium Version To Get More Features ==

[Premium Version](http://codecanyon.net/item/rnb-woocommerce-rental-booking/14835145?ref=redqteam "premium version") of Booking and Rental System(Woocommerce) is completely different from the free version as there are a lot more features included.

= Pro Version Demo : <a href="http://codecanyon.net/item/rnb-woocommerce-rental-booking/full_screen_preview/14835145?ref=redqteam">Online Demo</a> of the Pro version. =
= Admin Demo For Pro Version : Check <a href="http://demo.redq.io/rnbdemo/wp-login.php">Online Admin Demo</a> for Pro version. =


= Features Offered By The Free Version Of This Plugin =

*   Manage Booking Cost Easily
*   Individual Booking Cost Per Product
*   Booking Availability from Backend
*   Booking availability shows in front-end
*   Adding unlimited product attriblues
*   Adding unlimited product features
*   Drag & Drop Functionality for arrtibutes and featues
*   Custom Order Message via E-mail
*   Apply Coupon code/ Discount Code of Woocommerce


= To Solve Your Searching & Filtering Problems =

Visit Our Amazing [ Searching & Filtering ](https://codecanyon.net/item/reactive-pro-advanced-wordpress-search-filtering-grid/17425763?ref=redqteam "Premium Searching Plugin") Plugin on CodeCanyon.


= Features For Pro-version of Rental Plugin =

* Unlimited Bookable Products [cool]
* Inventory Managements [hot]
* Product Availability Control [hot]
* Request For Quote [super hot]
* Price Discount [cool]
* Various Date Format
* General Price Configuration [hot]
* Daily Price Configuration [hot]
* Monthly Price Configuration [hot]
* Day Ranges Price Configuration. [hot]
* Hourly Price Configuration
* Pickup/Return Locations Cost [cool]
* Unlimited Payable Resources [cool]
* PerDay/OneTime Basis Resource Cost
* Unlimited Payable/Non-payable Categories [hot]
* Set Quantity on Category
* PerDay/OneTime Basis Category Cost
* Choose Category as Mandatory/Optional
* Unlimited Payable Deposits [cool]
* PerDay/OneTime Basis Deposits [cool]
* Choose Deposit as Mandatory/Optional
* Unlimited Payable/Non-payable Person [cool]
* Select Person As Adult/Child
* PerDay/OneTime Basis Person Cost
* Local/Global Settings Panel [hot]


## Integrations

* Full Calendar Orders Report [hot]
* Google Calendar Order Synchronization [hot]
* Various Email Notifications Depending on Order Status [hot]
* WPML Ready [hot]
* Multilingual/Translation Ready [hot]

=  Wordpress Theme Developed On The Top Of This Booking & Filtering Plugin  =

Visit [ Wordpress Theme ](https://themeforest.net/item/turbo-car-rental-system-wordpress-theme/17156768?ref=redqteam "Premium Wordpress Theme") developed on the top of this plugin.




= Our Portfilio =
You can check our <a href="http://codecanyon.net/user/redqteam/portfolio?ref=redqteam">Portfolio</a> from here.

== Installation ==

1. To install the plugin go to the wordpress codex: http://codex.wordpress.org/Managing_Plugins or you can install manually http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation


== Frequently Asked Questions ==
= FAQ =


== Screenshots ==

1. screenshot-1.png
2. screenshot-2.png
3. screenshot-3.png
4. screenshot-4.png
5. screenshot-5.png

== Changelog ==
Version 1.0.2 ( 03 June, 2017 )

- Full Compatible with WooCommerce 3.0
- Make compatibility with WooCommerce Defaults Products
- Some CSS issues solved

Version 1.0.3 ( 03 June, 2017 )

- Order meta fixed
- Product tabs fixed

