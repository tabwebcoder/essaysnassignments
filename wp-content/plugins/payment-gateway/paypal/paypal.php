<?php
//require "app/start.php";
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item; 
use PayPal\Api\ItemList; 
use PayPal\Api\Payer; 
use PayPal\Api\Payment; 
use PayPal\Api\RedirectUrls; 
use PayPal\Api\Transaction;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\FundingInstrument;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
set_time_limit(3600);


function execute_payment($response)
{
    $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                get_option('pgs_paypal_client_id'),
                get_option('pgs_paypal_client_secret')
            )
    );
    if (isset($response['success']) && $response['success'] == 'true') {

        $paymentId = $response['paymentId'];
        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($response['PayerID']);

        try {
            
            $result = $payment->execute($execution, $apiContext);
            
            try {
                $payment = Payment::get($paymentId, $apiContext);

                    $resp = $payment->toArray();
                    $Response_paypal = json_encode( $resp);
                    $TransactionDetails = $resp['transactions'][0];
                    $TransId = $resp['id'];
                    $status['msg']      = 'success';
                    $status['TransactionDetail'] = $TransactionDetails;
                    $status['TransactionId'] = $TransId;
                  return  $status;

            } catch (Exception $ex) {
                $error = $ex->getData();
                $status['msg']      = 'fail';
                $status['data']     = $error;
                return $status;
            }
        } catch (Exception $ex) {
            $error = $ex->getData();
            $status['msg']      = 'fail';
            $status['data']     = $error;
            return $status;
        }
            $error = $ex->getData();
            $status['msg']      = 'fail';
            $status['data']     = $error;
    } else {
        $status['msg']      = 'fail';
        $status['data']     = "User Cancelled the Approval";
    }
    return $status;
}

function create_payment_url($charge_amount, $currency, $details = array())
{
    $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                get_option('pgs_paypal_client_id'),
                get_option('pgs_paypal_client_secret')
            )
    );
    
    $paypalRedirectSuccess = get_option('pgs_paypal_redirect_success');
    
    $paypalRedirectCancel = get_option('pgs_paypal_redirect_cancel');
    
    $payer = new Payer();
    $payer->setPaymentMethod("paypal");

    $amount = new Amount();
    $amount->setCurrency($currency)
        ->setTotal($charge_amount);


    $item = new Item();

    $item->setDescription(json_encode($details))->setCurrency($currency)->setQuantity(1)->setPrice($charge_amount);
    $itemList = new ItemList();
    $itemList->setItems(array($item));
    
    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription("Payment description")
        ->setInvoiceNumber(uniqid());

    $baseUrl = getBaseUrl();
    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl($paypalRedirectSuccess."/?success=true")
        ->setCancelUrl($paypalRedirectCancel."/?success=false");

    $payment = new Payment();
    $payment->setIntent("sale")
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

    try {
        $payment->create($apiContext);
    } catch (Exception $ex) {
        $error = $ex->getData();
        $status['msg']      = 'fail';
        $status['data']     = $error;
         return $status;
    }
   
    $approvalUrl = $payment->getApprovalLink();
    return $approvalUrl;
}



function getBaseUrl()
{


    if($_SERVER['SERVER_NAME'] == "localhost"){
        $web_url='http://'.$_SERVER['SERVER_NAME']."/outside/essay-site/wp/";
    }
    else
    {
        $web_url='http://globalmarketingforum.org/mhrwriter';
    }

   // global $web_url;
    return $web_url;
    
}