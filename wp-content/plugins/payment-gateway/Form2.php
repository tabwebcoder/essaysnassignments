<div class="avada-row">
    <div class="fusion-three-fourth three_fourth fusion-layout-column fusion-column spacing-yes">
        <div class="fusion-column-wrapper">
            <div id="myorder">
                <h1>Order Now for Writing Service</h1>
                <div id="Order">
                    <div class="loader"><img src="/mhrwriter/wp-content/themes/mhrwriter/images/ajax-loader.gif" alt="AJAX Loading" width="100" height="100"></div>


                    <form name="ordernow" id="ordernow" method="post" onsubmit="return ValidateOrder()">
                        <!-- cyber source hidden values-->
                        <input type="hidden" name="access_key" value="<?=get_option('wds_settings_cs_access_key')?>">
                        <input type="hidden" name="profile_id" value="<?=get_option('wds_settings_cs_profile_id')?>">
                        <input type="hidden" name="transaction_uuid" value="<?php echo uniqid() ?>">
                        <input type="hidden" name="signed_field_names"
                               value="merchant_defined_data1,consumer_id,contact_phone,email_address,access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency">
                        <input type="hidden" name="unsigned_field_names">
                        <input type="hidden" name="consumer_id" value="<?php //echo date_timestamp_get($date); ?>">
                        <input type="hidden" name="customer_ip_address"
                               value="<?php echo $_SERVER['REMOTE_ADDR'] ?>">
                        <input type="hidden" name="merchant_defined_data1" value="WC">
                        <input type="hidden" name="signed_date_time"
                               value="<?php echo gmdate("Y-m-d\TH:i:s\Z"); ?>">
                        <input type="hidden" name="locale" value="en">
                        <input type="hidden" name="transaction_type" size="25" value="sale">
                        <input type="hidden" name="reference_number"
                               value="<?php //echo date_timestamp_get($date); ?>">
                        <!-- end of hidden fields -->
                        <h3>Customer Information:</h3>
                        <br>
                        <div class="form-row">
                            <div class="field-label">Full Name <span>*</span></div>
                            <div class="field-input">
                                <input type="text" name="full_name">
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Email <span>*</span></div>
                            <div class="field-input">
                                <input type="text" name="email_address">
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Contact Phone <span>*</span></div>
                            <div class="field-input">
                                <input type="text" name="contact_phone">
                            </div>
                            <p></p>
                        </div>
                        <h3 class="top">Order Pricing:</h3>
                        <br>
                        <div class="form-row">
                            <div class="field-label">Type of Paper <span>*</span></div>
                            <div class="field-input">
                                <select name="paper_type">
                                    <option value="<?php echo $get_fixed_link_data['paper_type_id']; ?>"><?php echo $get_fixed_link_data['paper_types_name']; ?></option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Type of Service <span>*</span></div>
                            <div class="field-input">
                                <select name="service_type">
                                     <option value="<?php echo $get_fixed_link_data['service_type_id']; ?>"><?php echo $get_fixed_link_data['service_types_name']; ?></option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Urgency <span>*</span></div>
                            <div class="field-input">
                                <select name="urgency_time" class="shorts">
                                     <option value="<?php echo $get_fixed_link_data['urgency_id']; ?>"><?php echo $get_fixed_link_data['urgency_name']; ?></option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Quality Level <span>*</span></div>
                            <div class="field-input">
                                <select name="quality_level" class="shorts">
                                   <option value="<?php echo $get_fixed_link_data['quality_level_id']; ?>"><?php echo $get_fixed_link_data['quality_levels_name']; ?></option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Number of Pages <span>*</span></div>
                            <div class="field-input">
                                <select name="number_of_pages" class="shorts">
                                     <option value="<?php echo $get_fixed_link_data['no_pages_id']; ?>"><?php echo $get_fixed_link_data['no_pages_name']; ?></option>
                                    
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Currency <span>*</span></div>
                            <div class="field-input">
                                <select name="currency" class="shortest">
                                     <option value="<?php echo $get_fixed_link_data['currency']; ?>"><?php echo $get_fixed_link_data['currencies_name']; ?></option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Plagiarism Report? <span>*</span></div>
                            <div class="field-input">
                                <div class="plag-yes radio">
                                    <input type="radio" name="plag_report" id="yes" value="1">  <label for="yes">Yes</label><span class="plag_rate">(£4 for 4,000 words)</span></div>
                                <div class="plag-no radio">
                                    <input type="radio" name="plag_report" id="no" value="2" checked='checked'>  <label for="no">No</label>
                                </div>
                                <p></p>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Discount Code</div>
                            <div class="field-input">
                                <input type="text" name="discount_code" class="shortest" autocomplete="off" value="<?= (isset($_GET['dCode']) && $_GET['dCode'] != '' ? $_GET['dCode'] : ''); ?>">
                            </div>
                            <p></p>
                        </div>
                        <div class="discount_view"></div>
                        <div class="form-row">
                            <div class="field-label cost-label">Total <strong>Cost</strong></div>
                            <div class="field-written">
                                <span class="total">£<?php echo $get_fixed_link_data['amount']; ?><b  class="perpage">(select type of paper)</b></span>
                                <p></p>
                            </div>
                            <p></p>
                        </div>
                        <h3 class="top">Order Information:</h3>
                        <br>
                        <div class="form-row">
                            <div class="field-label">Subject Area <span>*</span></div>
                            <div class="field-input">
                                <select name="subject_area">
                                    <option value="0">[Not Selected]</option>

                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Your Topic <span>*</span></div>
                            <div class="field-input">
                                <input type="text" name="paper_topic">
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Attach Files</div>
                            <div class="field-input">
                                <input type="file" name="displayfiles" id="displayfiles" class="button2" value="Click to Browse">
                                <br>
                                <input type="file" name="files" id="fileuploadmanually" style="display:none;" multiple="">
                                <input type="hidden" value="" id="files_list" name="files_list">
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Academic Level <span>*</span></div>
                            <div class="field-input">
                                <select name="academic_level" class="shortest">
                                    <option value="0">[Not Selected]</option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Writing Style <span>*</span></div>
                            <div class="field-input">
                                <select name="writing_style" class="shortest">
                                    <option value="0">[Not Selected]</option>

                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Preferred Language <span>*</span></div>
                            <div class="field-input">
                                <select name="preferred_language" class="shorts">
                                    <option value="0">[Not Selected]</option>
                                    <option value="English (U.K)">English (U.K)</option>
                                    <option value="English (U.S.A)">English (U.S.A)</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Required References <span>*</span></div>
                            <div class="field-input">
                                <select name="no_of_sources" class="shorts">
                                    <option value="0">[Not Selected]</option>
                                    <option value="Not Required">Not Required</option>
                                    <?php
                                        for ($i=1; $i < 81; $i++) { 
                                            ?>
                                              <option value="<?php echo $i ?>"><?php echo $i ?></option>
                                            <?php 
                                            # code...
                                        }
                                     ?>
                                    
                                </select>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">Detailed Instructions <span>*</span></div>
                            <div class="field-input">
                                <textarea name="any_specification" rows="7" cols="12"></textarea>
                            </div>
                            <p></p>
                        </div>
                        <div class="form-row">
                            <div class="field-label">How Did You Find Us <span>*</span></div>
                            <div class="field-input">
                                <select name="reach_us_sources" class="shorts">
                                    <option value="0">[Not Selected]</option>
                                    <option value="2">Social Media (Facebook, Twitter, Instagram)</option>
                                    <option value="3">Search Engine (Google, Bing)</option>
                                    <option value="4">Refer by a Friend</option>
                                    <option value="5">Returning Customer</option>
                                    <option value="6">Email</option>
                                    <option value="7">Online Advertisement</option>
                                    <option value="8">Newspaper or TV Ads</option>
                                    <option value="9">Billboard or Flyer</option>
                                    <option value="1">Other</option>
                                </select>
                            </div>
                            <p></p>
                        </div>

                        <div class="form-row">
                            <div class="field-label">Payment Method <span>*</span></div>
                            <div class="field-input">
                                <div class="pm-cc-pp radio">
                                    
                                    <?php if(get_option('pgs_stripe_status') == 'enable'){ ?>
                                        <input type="radio" name="pay_method" id="cc" value="Stripe">
                                        <label for="cc">Credit Card( StripeCreditCharge )</label>
                                        &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                    <?php } ?>
                                        
                                    <?php if(get_option('pgs_paypal_status') == 'enable'){ ?>
                                        <input type="radio" name="pay_method" id="pp" value="Paypal">
                                        <label for="pp">Paypal</label>
                                        &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                    <?php } ?>

                                </div>

                            </div>
                            <p></p>
                        </div>

                        <div class="form-row">


                            <input type="checkbox" name="agree_terms" id="agree_terms">  <label for="agree_terms">I have read and agree to <a href="/mhrwriter/guarantee" class="style" target="_blank">Terms &amp; Policies</a>.</label>


                            <br>
                            <input type="checkbox" name="agree_2co" id="agree_2co"> <label for="agree_2co">I accept that Paypal or Card payment can be processed via Stripe.com (our payment processor) which is safe and secure.</label>
                        </div>
                        <div class="form-row">
                            <div class="field-label"></div>
                            <div class="field-input">
                                <input type="hidden" name="amount" class="total_amount">
                                <button name="wds_donate" value="place_order" type="submit">Place Order</button>
                            </div>
                            <p></p>
                        </div>
                        <p></p>
                    </form>
                    <p></p>
                </div>
                <h3 class="heading3">Secure Order Process:</h3>
                <br>
                <p>People are usually concerned at the time of placing an order whether the site is secure and trustworthy or not. Once the payment has been made the thought might enter a student’s mind, what if I don’t get satisfied work or if I have plagiarism issues? The entire team of MHR Writer would like to take this opportunity to remind you that we are here to serve you. According to the Satisfaction Guarantee you will be able to get unlimited revisions if our writers are unable to thoroughly satisfy your initial demands. You can also avail the option of revision or a refund IF there is any plagiarism in the final document.</p>
                <p>If you have placed an order and you want to cancel it, you will be issued a refund. If there are any issues or concerns you can contact the customer support service team at MHR Writer to get personalised assistance. They will be more than happy to assist you.</p>
                <p>Know that with <a href="/mhrwriter/" title="MHR Writing Solutions">MHR Writer</a> you are in good hands. Our goals at the time of composing the terms and conditions is maintaining long term relationships with all of our customers. We will strictly comply with all of the terms and policies to facilitate you. We are enthusiastic in providing learners with quality services each time.</p>
            </div>
        </div>
    </div>
    <div class="fusion-one-fourth one_fourth fusion-layout-column fusion-column last spacing-yes">
        <div class="fusion-column-wrapper">
            <div id="SN">
                <div class="sitebox features">
                    <div class="top">
                        <div class="head">Benefits You Get</div>
                        <div class="bg"><span>Get it<br>NOW</span></div>
                        <p></p>
                    </div>
                    <div class="bottom">
                        <ul>
                            <li>Customer support 24/7</li>
                            <li>100% satisfaction guaranteed</li>
                            <li>100% confidentiality</li>
                            <li>On-time help provided</li>
                            <li>Contact directly to helper</li>
                            <li>Unique ideas and thoughts</li>
                        </ul>
                    </div>
                    <p></p>
                </div>
                <p>
                    <a href="/mhrwriter/order" title="Order Now for Online Assignment Help" class="order"> <img src="/mhrwriter/wp-content/uploads/2018/08/order-now.png" alt="Order Today for Quality Assignment Writing Services" width="270" height="71"> </a>
                </p>
                <div class="sitebox livechat" onclick="Comm100API.open_chat_window(event, 366);">
                    <div class="top">
                        <div class="head"><span>Live Chat</span><span class="status">ONLINE</span></div>
                        <div class="bg"><span>Chat<br>NOW</span></div>
                        <p></p>
                    </div>
                    <div class="bottom">
                        <p>Let us Gladly
                            <br>Assist you…!!</p>
                        <p></p>
                    </div>
                    <p></p>
                </div>
                <div class="sitebox callnow">
                    <div class="top">
                        <div class="head">24/7 UK Toll Free</div>
                        <div class="bg"><span>Call<br>NOW</span></div>
                        <p></p>
                    </div>
                    <div class="bottom">+44 800 048 8966</div>
                    <p></p>
                </div>
                <div class="sitebox blogs">
                    <div class="top">
                        <div class="head">Latest Blogs</div>
                        <p></p>
                    </div>
                    <div class="bottom">
                        <ul>
                            <li> <a href="/mhrwriter/how-to-overcome-shyness">How to Overcome Shyness When Meeting New Acquaintances</a> </li>
                            <li> <a href="/mhrwriter/productive-homework-tips-for-students">3 Productive Homework Tips for Students to Achieve Success</a> </li>
                            <li> <a href="/how-to-improve-public-speaking-skills">How to Improve Public Speaking Skills and Confidence</a> </li>
                            <li> <a href="/mhrwriter/ways-to-save-money-to-manage-expenses">Easy Ways to Save Money to Manage Academic Expenses</a> </li>
                            <li> <a href="/mhrwriter/dissertation-vs-thesis-uk">Dissertation VS Thesis Know the Similarities and Differences</a> </li>
                            <li> <a href="/mhrwriter/how-to-concentrate-on-studies-and-retain-a-job">How to Concentrate on Studies and Successfully Retain a Job</a> </li>
                            <li> <a href="/mhrwriter/how-to-write-a-dissertation">How to Write a Dissertation and Concurrently Seek Employment</a> </li>
                            <li> <a href="/mhrwriter/controlling-the-average-student-budget">Controlling the Average Student Budget for Recreational Purposes</a> </li>
                            <li> <a href="/mhrwriter/change-wording-to-avoid-plagiarism">Productively Change Words to Avoid Plagiarism in Your Work</a> </li>
                            <li> <a href="/mhrwriter/how-to-start-writing-a-dissertation">How to Start a Dissertation and the Measures Involved</a> </li>
                        </ul>
                    </div>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
    <div class="fusion-clearfix"></div>
</div>
<!-- js scripts -->
<script>var get_dcode_param = '<?= (isset($_GET['dCode']) && $_GET['dCode'] != '' ? $_GET['dCode'] : ''); ?>';</script>
<script defer="defer" src="/wp-content/plugins/payment-gateway/js/all.js"></script>
<script defer="defer" type="text/javascript"> var service_type_id = 0; var urgency_time_id = 0; </script>
<script defer="defer" type="text/javascript" src="/wp-content/plugins/payment-gateway/js/order-data.js">
</script> <script defer="defer" type="text/javascript"> var TemplateUrl = "/wp-content/themes/EssaysnAssignments/"; var NoCalculate = ""; var SeparateUpload = "no"; var plag_price = "4"; var plag_words = "4000"; </script>
<script defer="defer" type="text/javascript" src="/wp-content/plugins/payment-gateway/js/attach-files.js"></script>
<script defer="defer" type="text/javascript" src="/wp-content/plugins/payment-gateway/js/order_fixed.js"></script>
