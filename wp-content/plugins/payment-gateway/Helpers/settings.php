<div class="wrap">
    <h3><?php _e('PaymentGateway API Settings', 'PaymentGateway'); ?></h3>

    <?php
    if (isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true'):
        echo '<div id="setting-error-settings_updated" class="updated settings-error">
        <p><strong>Settings saved.</strong></p></div>';
    endif;
    ?>
    <form method="post" action="options.php">
        
        <?php settings_fields('PaymentGateway'); ?>
        <?php do_settings_sections('PaymentGateway'); ?>
        
        <table class="form-table">
        <tr valign="top" class="show-duration">
            <th scope="row">Select Environment </th>
            <td>
                <select name="pgs_env" >
                <option value="Sandbox" <?php if( get_option('pgs_env') == "Sandbox" ): echo 'selected'; endif;?> >Sandbox</option>
                <option value="Live" <?php if( get_option('pgs_env') == "Live" ): echo 'selected'; endif;?> >Live</option>
                </select>
            </td>
        </tr>
        <tr valign="top" class="show-duration">
            <th scope="row">Add Info Email</th>
            <td>
                <input type="text" style="width:50%" name="pgs_info_email" value="<?php echo get_option('pgs_info_email'); ?>" placeholder="Info Email" />
            </td>
        </tr>
        <tr valign="top" class="show-duration">
            <th scope="row">Add Support Email </th>
            <td>
                <input type="text" style="width:50%" name="pgs_support_email" value="<?php echo get_option('pgs_support_email'); ?>" placeholder="Support Email" />
            </td>
        </tr>
        </table>
        
        <h4>Paypal Settings</h4>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Paypal Client Id</th>
                <td>
                    <input type="text" style="width:50%" name="pgs_paypal_client_id" value="<?php echo get_option('pgs_paypal_client_id'); ?>" placeholder="Paypal Client Id" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Paypal Client Secret</th>
                <td>
                    <input type="text" style="width:50%" name="pgs_paypal_client_secret" value="<?php echo get_option('pgs_paypal_client_secret'); ?>" placeholder="Paypal Client Secret" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Paypal Redirect Success</th>
                <td>
                    <input type="text" style="width:50%" name="pgs_paypal_redirect_success" value="<?php echo get_option('pgs_paypal_redirect_success'); ?>" placeholder="Paypal Redirect Success" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Paypal Redirect Cancel</th>
                <td>
                    <input type="text" style="width:50%" name="pgs_paypal_redirect_cancel" value="<?php echo get_option('pgs_paypal_redirect_cancel'); ?>" placeholder="Paypal Redirect Cancel" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Paypal Status</th>
                <td>
                    <select name="pgs_paypal_status" >
                        <option value="enable" <?php if( get_option('pgs_paypal_status') == "enable" ): echo 'selected'; endif;?> >Enable</option>
                        <option value="disable" <?php if( get_option('pgs_paypal_status') == "disable" ): echo 'selected'; endif;?> >Disable</option>
                    </select>
                </td>
            </tr>
        </table>
        
        <h4>Stripe Settings</h4>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Stripe Client Id</th>
                <td>
                    <input type="text" style="width:50%" name="pgs_stripe_client_id" value="<?php echo get_option('pgs_stripe_client_id'); ?>" placeholder="Stripe Client Id" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Stripe Secret Key</th>
                <td>
                    <input type="text" style="width:50%" name="pgs_stripe_client_secret" value="<?php echo get_option('pgs_stripe_client_secret'); ?>" placeholder="Stripe Secret Key" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Stripe Test Client Id</th>
                <td>
                    <input type="text" style="width:50%" name="pgs_stripe_client_id_test" value="<?php echo get_option('pgs_stripe_client_id_test'); ?>" placeholder="Stripe Test Client Id" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Stripe Test Secret Key</th>
                <td>
                    <input type="text" style="width:50%" name="pgs_stripe_client_secret_test" value="<?php echo get_option('pgs_stripe_client_secret_test'); ?>" placeholder="Stripe Test Secret Key" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Stripe Status</th>
                <td>
                    <select name="pgs_stripe_status" >
                        <option value="enable" <?php if( get_option('pgs_stripe_status') == "enable" ): echo 'selected'; endif;?> >Enable</option>
                        <option value="disable" <?php if( get_option('pgs_stripe_status') == "disable" ): echo 'selected'; endif;?> >Disable</option>
                    </select>
                </td>
            </tr>
        </table>
        
        <?php submit_button(); ?>
        
    </form>
</div>
