<?php
/**
 * Created using Netbeans.
 * User: Awais-Mustafa
 * Date: 3/30/2020
 */

#namespace Helpers\Pm;


#use Helpers\Gateways\Paypal;
#use Helpers\Gateways\StripeCreditCharge;
require_once("Gateways/Paypal.php");
require_once("Gateways/StripeCreditCharge.php");
class PaymentMethod {

    function getPaymentGateway($paymentGateway) {
        
        if($paymentGateway == 'Paypal'){
            return new Paypal();
        }
        
        else if($paymentGateway == 'StripeCreditCharge'){
            return new StripeCreditCharge();
        }
        
    }
}