<?php
/**
 * Created using Netbeans.
 * User: Awais-Mustafa
 * Date: 3/30/2020
 */

#namespace Payment\Helpers\Gateways;
define( 'MY_PLUGIN_PATH', plugin_dir_path( __DIR__ ) );
require_once(MY_PLUGIN_PATH . "../stripe/lib/Stripe.php");
class StripeCreditCharge {
    
    public $postData;
    public $env;
    public $url;
    public $stripeKeys;
    public $authorizeUrl = 'https://connect.stripe.com/oauth/authorize';
    public $tokenUri = 'https://connect.stripe.com/oauth/token';
    
    function gatewayProcess ($postData, $env, $cardData)
    {
        
        if (isset($cardData['cardNumber'])) {
            
            if($env == 'Sandbox'){
                
                $this->stripeKeys = [
                    'clientId' => get_option('pgs_stripe_client_id_test'), 
                    'clientSecret' => get_option('pgs_stripe_client_secret_test'), 
                ];
                
            }
            else {
                
                $this->stripeKeys = [
                    'clientId' => get_option('pgs_stripe_client_id'), 
                    'clientSecret' => get_option('pgs_stripe_client_secret'), 
                ];            
                
            }
            
            Stripe::setApiKey($this->stripeKeys['clientSecret']);
            
            //get currency
            $currencyList = [1=>'GBP',2=>'USD',3=>'EUR',4=>'AUD',5=>'CAD'];
            $txnCurrency  = $postData['currency'];
            $gatewayCurrency = $currencyList[$txnCurrency];

            //txn amount
            $txnAmount = round($postData['amount']);

            //txn details
            $emailAddress = $postData['email_address'];
            $txnDetails = array('email' => $emailAddress);

            //cvard holder details
            $cardHolderName = $cardData['cardHolderName'];
            $cardNumber = trim($cardData['cardNumber']);
            $expMonth = $cardData['expMonth'];
            $expYear = $cardData['expYear'];
            $cvcCode = $cardData['cvcCode'];
            $email = $cardData['email'];

            $tokenResponse = self::get_stripe_token($cardHolderName, trim($cardNumber), $expMonth, $expYear, $cvcCode);
            if (isset($tokenResponse['error']) && $tokenResponse['error'] == 'yes') { 

                $errorMessage = $tokenResponse['message'];
                return [
                    'error' => 1, 
                    'type' => 'error',
                    'message' => $errorMessage, 
                    'showPartial' => 'CreditCardForm.php'
                ];
                exit();

            } else { 

                $tokenArray = $tokenResponse->__toArray();
                $txnResponse = self::charge_amount($tokenArray, $txnAmount, $gatewayCurrency, $emailAddress);
                $txnResponse = $txnResponse->__toArray();
                if (isset($txnResponse['id'])) {

                    $responseJson = json_encode($txnResponse);
                    $orderId = $this->post_order_api($responseJson, 'completed', $txnResponse['id']);

                    return [
                        'error' => 0, 
                        'message' => '', 
                        'type' => 'done',
                        'orderId' => $orderId,
                        'showPartial' => 'thanks.php'
                    ];
                    exit();

                } else {

                    return [
                        'error' => 1, 
                        'type' => 'cancel',
                        'message' => '', 
                        'showPartial' => 'payment_cancle.php'
                    ];
                    exit();
                }

            }
        
        }
        else {
            
            return [
                'error' => 1, 
                'type' => 'error',
                'message' => '', 
                'showPartial' => 'CreditCardForm.php'
            ];
            exit;
            
        }
    }
    
    function post_order_api($gatewayRes, $txnStatus, $txnId = NULL) {


        $postData = $_SESSION['data'];
        $utilityClass = new Utility(); 
        $orderId = $utilityClass->pushDataViaApi($postData, $gatewayRes, $txnStatus, $txnId);
        
        return $orderId;

    }
    
    static function get_stripe_token($name, $card, $expmonth, $expyear, $cvc = '') {

        try {
            $token = Stripe_Token::create(
                array("card" =>
                    array(
                        "name" => $name,
                        "number" => $card,
                        "exp_month" => $expmonth,
                        "exp_year" => $expyear,
                        "cvc" => $cvc
                    )
            ));

            if (!$token->error) {
                
                return $token;
                
            } else {

                $message = ['error' => 'yes', 'message' => $token->error['message']];
                return $message;

            }

        } catch (\Exception $ex) {
            return ['error' => 'yes', 'mesage' => $ex];
        }

    }
    
    static function get_auth_url() {


        $authorize_request_body = array(
            'response_type' => 'code',
            'scope' => 'read_write',
            'client_id' => $this->stripeKeys['clientId']
        );

        $url = $this->authorizeUrl . '?' . http_build_query($authorize_request_body);

        return $url;

    }
    
    static function get_user_details($code) {

        if (isset($code) && !empty($code)) {

            $token_request_body = array(

                'client_secret' => $this->stripeKeys['clientSecret'],
                'grant_type' => 'authorization_code',
                'client_id' => $this->stripeKeys['clientId'],
                'code' => $code,
            );

            $resp = self::send_curl($this->tokenUri, $token_request_body);
            return $resp;

        }

    }

    static function send_curl($url, $pram) {

        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POST, true);
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($pram));
        // TODO: Additional error handling

        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $resp = json_decode(curl_exec($req), true);
        curl_close($req);
        return $resp;

    }



    // THIS FUNCTION IS TO CHARGE ACCOUNT...

    static function charge_amount($token, $payment, $currency, $receipt_email='') {

        $payment *= 100;
        $payment = (int) $payment;
        //print_r($token);exit;
        if (!isset($token['error'])) {

            try {

                $token = $token['id'];
                
                // Create a Customer:
                $customer = Stripe_Customer::create(array(
                    "source" => $token,
                    'email'=> $receipt_email
                    )
                );

                // Charge the Customer instead of the card
                $charge = Stripe_Charge::create(array(
                    "amount" => $payment, 
                    "currency" => $currency, 
                    "customer" => $customer['id'],
                    "receipt_email"=>$receipt_email
                    )
                );

                if ($charge['error']) {

                    return ['error' => 'yes', 'mesage' => $charge['message']];

                } else {

                    return $charge;

                }

            } catch (\Exception $ex) {

                return ['error' => 'yes', 'mesage' => $ex];

            }

        } else {

        }

    }
    
    static function charge_customer($cus_id, $payment, $currency,$receipt_email ='') {

        $payment *= 100;
        $payment = (int) $payment;

        try {

            $charge = Stripe_Charge::create(array(
                "amount" => $payment, 
                "currency" => $currency, 
                "customer" => $cus_id,
                "receipt_email"=>$receipt_email
                )
            );

            if ($charge['error']) {
                
                return ['error' => 'yes', 'mesage' => $charge['message']];

            } else {

                return $charge;

            }

        } catch (\Exception $ex) {

            return ['error' => 'yes', 'mesage' => $ex];

        }

    }



    static function create_customer($token, $desc = 'essay_customer') {

        try {

            $customer = Stripe_Customer::create(array(
                "description" => $desc,
                "source" => $token['id']
            ));

            return $customer;

        } catch (\Exception $ex) {

            return ['error' => 'yes', 'mesage' => $ex];

        }

    }



    static function transfer_amount($amount, $currency, $acc_id, $desc) {

        try {

            $amount = $amount * 100;

            return Stripe_Transfer::create(array(
                "amount" => $amount,
                "currency" => $currency,
                "destination" => $acc_id,
                "description" => $desc
            ));

        } catch (\Exception $ex) {

            return ['error' => 'yes', 'mesage' => $ex];

        }

    }
    
}