<?php
/**
 * Created using Netbeans.
 * User: Awais-Mustafa
 * Date: 3/30/2020
 */

#namespace Payment\Helpers\Gateways\Paypal;
define( 'MY_PLUGIN_PATH', plugin_dir_path( __DIR__ ) );


class Paypal {
    
    
    function gatewayProcess ($postData, $paymentMode, $cardData = NULL)
    {
        require_once(MY_PLUGIN_PATH . "../paypal_process.php");
        //get currency
        $currencyList = [1=>'GBP',2=>'USD',3=>'EUR',4=>'AUD',5=>'CAD'];
        $txnCurrency  = $postData['currency'];
        $gatewayCurrency = $currencyList[$txnCurrency];
        
        //txn amount
        $txnAmount = round($postData['amount']);
        
        //txn details
        $emailAddress = $postData['email_address'];
        $txnDetails = array('email' => $emailAddress);
        
        //prepare paypal url
        $preparePaypalUrl = create_payment_url($txnAmount, $gatewayCurrency, $txnDetails);
        
        if ($paymentMode == 'Sandbox') {
            $paypalUrl = str_replace("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=", "", $preparePaypalUrl);
        }
        
        else {
            $paypalUrl = str_replace("https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=", "", $preparePaypalUrl);
        }
        
        echo '<script type="text/javascript">window.location = "' . $preparePaypalUrl . '"</script>';
        
        
    }
    
}