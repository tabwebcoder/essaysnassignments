<?php
/**
 * Created using Netbeans.
 * User: Awais-Mustafa
 * Date: 3/30/2020
 */

#namespace Helpers\Ut;
define( 'MY_PLUGIN_PATH', plugin_dir_path( __DIR__ ) );

class Utility {
    
    function pullFixedLinksData($code){
        
        $apiUser = "admin";
        $apiPass = "tl001";
        $baseUrl = get_option('siteurl') . '/';
        
        $params = [
            'code' => $code
        ];
        
        //print_r($params);exit;
        $postString = "";
        foreach( $params as $key => $value )
        { $postString .= "$key=" . urlencode( $value ) . "&"; }
        $postString = rtrim( $postString, "& " );
        $request = curl_init($baseUrl."wpanel/backend/web/index.php/essay-api/fixed-links");

        curl_setopt($request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_USERPWD, $apiUser . ":" . $apiPass);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $postString);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);      
        curl_setopt($request, CURLOPT_POST, 1);

        $post_response = curl_exec($request);
        // Connection to Authorize.net
        curl_close ($request); // close curl object
        $curlOutout = json_decode($post_response, true);
        return $curlOutout;
        
        
    }

    function pushDataViaApi($postData, $gatewayRes, $txnStatus = 'pending', $txnId=NULL) {
        
        $apiUser = "admin";
        $apiPass = "tl001";
        
        $infoEmail = get_option('pgs_info_email');
        $supportEmail = get_option('pgs_support_email');
        
        $params = [
            'full_name' => $postData['full_name'],
            'email_address' => $postData['email_address'],
            'contact_phone' => $postData['contact_phone'],
            'customer_ip_address' => $_SERVER['REMOTE_ADDR'],
            'status' => 'pending',
            'total_amount' => $postData['amount'],
            'paper_type_id' => $postData['paper_type'],
            'service_type_id' => $postData['service_type'],
            'order_date' => date("Y-m-d H:i:s"),
            'urgency_id' => $postData['urgency_time'],
            'quality_id' => $postData['quality_level'],
            'no_pages_id' => $postData['number_of_pages'],
            'subject_area_id' => $postData['subject_area'],
            'topic' => $postData['paper_topic'],
            'acadamic_level_id' => $postData['academic_level'],
            'writing_style_id' => $postData['writing_style'],
            'language' => $postData['preferred_language'],
            'plagiarism_report' => $postData['plag_report'],
            'currency' => $postData['currency'],
            'referances' => '',
            'instructions' => $postData['any_specification'],
            'payment_method' => $postData['pay_method'],
            'transaction_id' => $txnId,
            'payee_name' => $postData['full_name'],
            'payee_amount' => $postData['amount'],
            'fraud_status' => 'Not Received',
            'date' => date("Y-m-d H:i:s"),
            'response' => $gatewayRes,

        ];
        
        //print_r($params);exit;
        $postString = "";
        foreach( $params as $key => $value )
        { $postString .= "$key=" . urlencode( $value ) . "&"; }
        $postString = rtrim( $postString, "& " );
        $request = curl_init($baseUrl."wpanel/backend/web/index.php/essay-api/create");

        curl_setopt($request, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_USERPWD, $apiUser . ":" . $apiPass);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $postString);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);      
        curl_setopt($request, CURLOPT_POST, 1);

        $post_response = curl_exec($request);
        // Connection to Authorize.net
        curl_close ($request); // close curl object
        $curlOutout = json_decode($post_response, true);
        //print_r($curlOutout);exit;
        //send customer emails
        $customerEmails = $curlOutout['customer_emails'];
        if(count($customerEmails)):
        foreach($customerEmails as $key=>$cEmail){
            $this->sendEmail($curlOutout['email_details']['emailTo'], $cEmail['emailBody'], $cEmail['emailSubject'], $supportEmail);            
            
        }
        endif; 
        
        //send admin email
        $this->sendEmail($infoEmail, $curlOutout['email_details']['adminEmailBody'], $curlOutout['email_details']['adminEmailSubject'], $supportEmail, $curlOutout['email_details']['adminEmailBcc']);
        
        return $curlOutout['customer_emails'][1]['emailBody'];
    }
    
    function sendEmail($to='', $body, $subject, $from, $bcc = '') {
        
        require_once(MY_PLUGIN_PATH . "../PHPMailer/PHPMailerAutoload.php");

        //simple phpmailer code
        $mail = new PHPMailer;
        
        $mail->From = $from;
        $mail->FromName = 'EssaynAssignments';
        
        $mail->addAddress($to);
        if($bcc != ''){
            $mail->addBCC($bcc);
        }
        $mail->isHTML(true);
        
        $mail->Subject = $subject;
        $mail->Body = html_entity_decode(html_entity_decode($body));
        $mail->send();

    }
    
    
}