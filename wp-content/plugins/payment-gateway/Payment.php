<?php

/**
 * Plugin Name:       Payment gateway
 * Description:       Payment method of paypal and stripe!
 * Version:           2.0.0
 * Author:            Awais Mustafa
* License:           GPL-2.0+
 */

#namespace Payment;

#use Helpers\PaymentMethod;
#use Helpers\Utility;
/*
 * Plugin constants
 */
if (!defined('PaymentGateway_URL'))
    define('PaymentGateway_URL', plugin_dir_url(__FILE__));
if (!defined('PaymentGateway_PATH'))
    define('PaymentGateway_PATH', plugin_dir_path(__FILE__));

/*
 * Main class
 */
/**
 * Class PaymentGateway
 *
 * This class creates the option page and add the web app script
 */
require_once("paypal/vendor/autoload.php");
require "paypal/paypal.php";
require_once('Helpers/PaymentMethod.php');
require_once('Helpers/Utility.php');

//start session
session_start();

class PaymentGateway {

    /**
     * PaymentGateway constructor.
     *
     * The main plugin actions registered for WordPress
     */
    public function __construct() {
        global $wpdb;
        // Admin page calls:
        add_action('admin_menu', array($this, 'addAdminMenu'));
        add_action('admin_init', array($this, 'pgs_register_mysettings'));
    }

    public function addAdminMenu() {
        add_menu_page(__('PaymentGateway', 'PaymentGateway'), __('PaymentGateway', 'PaymentGateway'), 'manage_options', 'PaymentGateway', array($this, 'adminLayout'), '');
    }
    
    public function pgs_register_mysettings() {

        //Common settings
        register_setting('PaymentGateway', 'pgs_stripe_client_id_test');
        register_setting('PaymentGateway', 'pgs_stripe_client_secret_test');
        register_setting('PaymentGateway', 'pgs_stripe_client_id');
        register_setting('PaymentGateway', 'pgs_stripe_client_secret');
        register_setting('PaymentGateway', 'pgs_stripe_status');
        
        register_setting('PaymentGateway', 'pgs_env');
        register_setting('PaymentGateway', 'pgs_info_email');
        register_setting('PaymentGateway', 'pgs_support_email');
        
        register_setting('PaymentGateway', 'pgs_paypal_client_id');
        register_setting('PaymentGateway', 'pgs_paypal_client_secret');
        register_setting('PaymentGateway', 'pgs_paypal_redirect_success');
        register_setting('PaymentGateway', 'pgs_paypal_redirect_cancel');
        register_setting('PaymentGateway', 'pgs_paypal_status');

    }

    public function adminLayout() {
        include "Helpers/settings.php";  
    }

}

/*
 * Starts our plugin class, easy!
 */
new PaymentGateway();

function post_order_api($gatewayRes, $txnStatus, $txnId = NULL) {
    
    
    $postData = $_SESSION['data'];
    $utilityClass = new Utility(); 
    $orderId = $utilityClass->pushDataViaApi($postData, $gatewayRes, $txnStatus, $txnId);
    
    return $orderId;
    
}

function get_fixed_links($code) {
    
    $utilityClass = new Utility(); 
    $rowFixedLinks = $utilityClass->pullFixedLinksData($code);
    return $rowFixedLinks;
    
}

function InsertTempOrder($postData, $token = NULL) {
    
    //save data in session
    $_SESSION['data'] = $postData;
    $_SESSION['response'] = $token;
    
}

function form_creation() {
    
    if (isset($_GET['code']) && !empty($_GET['code']) && !isset($_POST['pay_method'])) {
        
        $code = $_GET['code'];
        $get_fixed_link_data = get_fixed_links($code);
        require "Form2.php";
        
    } else {
        
        #require "paypal_process.php";        
        
        //get payment mode
        $paymentMode = get_option('pgs_env');
        $selectedPayMethod = $_POST['pay_method'];
        $cardData = [];
        
        if(isset($selectedPayMethod)){ 
            if($selectedPayMethod == 'Stripe'){ 
                
                $postData = $_POST;
                InsertTempOrder($postData);
                $txAmount = $_POST['amount'];
                $emailAdd = trim($_POST['email_address']);
                $selectedCurrency = trim($_POST['currency']);
                require "CreditCardForm.php";    
                //exit;

            }
            else { 
                

                //save data into session
                if($selectedPayMethod != 'StripeCreditCharge'){
                    $postData = $_POST;
                    InsertTempOrder($postData);
                }
                else {
                    
                    $postData = $_SESSION['data'];
                    
                    if(isset($_POST['card-holder-name'])):
                    //card holder details
                    $cardData['cardHolderName'] = $_POST['card-holder-name'];
                    $cardData['cardNumber'] = trim($_POST['card-numbers']);
                    $cardData['expMonth'] = $_POST['expiry-month'];
                    $cardData['expYear'] = $_POST['expiry-year'];
                    $cardData['cvcCode'] = $_POST['cvv'];
                    $cardData['email'] = $_POST['email'];
                    endif; 
                    
                }
                

                //call payment gateway process
                $paymentGateway = new PaymentMethod();       
                $gatewayObject = $paymentGateway->getPaymentGateway($selectedPayMethod);    
                $gatewayResponse = $gatewayObject->gatewayProcess($postData, $paymentMode, $cardData);

                //print_r($gatewayResponse);exit;
                if(isset($gatewayResponse['error'])){

                    if($gatewayResponse['error'] == 1){
                        $message = $gatewayResponse['message'];
                        require $gatewayResponse['showPartial'];
                    }
                    else {                    
                        $orderId = $gatewayResponse['orderId'];
                        require $gatewayResponse['showPartial'];

                    }

                }
            }
        }
        else {
            require "Form.php";
            
        }

    }
}

add_shortcode('PaymentGatewayForm', 'form_creation');

function payment_thanks() {
    
    $Response = json_encode($_REQUEST);
    $response = execute_payment($_REQUEST);
    if (isset($response['msg']) && $response['msg'] == 'success') {
        
        $txnId = $response['TransactionId'];
        $orderId = post_order_api($Response, 'completed', $txnId);
        require "thanks.php";
        
    } else {
        
        require "payment_cancle.php";
        
    }
    
}

add_shortcode('PaymentGatewayThanks', 'payment_thanks');

function payment_cancle() {
    
    require "payment_cancle.php";
    
}

add_shortcode('PaymentGatewayCancle', 'payment_cancle');
