<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 25/08/17
 * Time: 10:09 PM
 */


namespace Donations\Authorizenet;

use Donations\Common\Classes\PaymentForm;
use Donations\Common\Interfaces;

class AuthorizeNet implements Interfaces\IPayment {
    var $form;
    private $api;
    private $credentials;
    private $amount;

    function __construct(PaymentForm $form, $environment) {
        $this->form = $form;
        $this->credentials = $environment == Interfaces\IPayment::ENVIRONMENT_LIVE
            ? $this->getLiveCredentials()
            : $this->getSandboxCredentials();

        $this->amount = $form->getUpsellProduct() == "1"
            ? get_option('wds_donation_authorizenet_amount') + get_option('wds_settings_upsell_amount')
            : get_option('wds_donation_authorizenet_amount');
        $this->api = new API($this->credentials['url']);
    }

    function makePayment() {
        $post_values = array(

            "x_login"			=> get_option('wds_donation_authorizenet_merchant_id'),
            "x_tran_key"		=> get_option('wds_donation_authorizenet_transaction_key'),

            "x_version"			=> "3.1",
            "x_delim_data"		=> "TRUE",
            "x_delim_char"		=> "|",
            "x_relay_response"	=> "FALSE",

            "x_type"			=> "AUTH_CAPTURE",
            "x_method"			=> "CC",
            "x_card_num"		=> $this->form->getPaymentForm()->getCreditCardNumber(),
            "x_card_code"		=> $this->form->getPaymentForm()->getCvv(),
            "x_exp_date"		=> $this->form->getPaymentForm()->getExpiry(),

            "x_amount"			=> $this->amount,
            "x_description"		=> get_option('wds_processor_description'),

            "x_first_name"		=> $this->form->getDonorFirstName(),
            "x_last_name"		=> $this->form->getDonorLastName(),
            "x_email"			=> $this->form->getDonorEmail(),
            "x_address"			=> $this->form->getDonorAddress(),
            "x_state"			=> $this->form->getDonorState(),
"x_phone"			=> $this->form->getDonorPhone(),
"x_city"			=> $this->form->getDonorCity(),
            "x_zip"				=> $this->form->getDonorZipCode()
        );

        if($this->form->getThreeDsForm() != null) {
            $post_values['x_authentication_indicator'] = $this->form->getThreeDsForm()->getEciFlag();
            $post_values['x_cardholder_authentication_value'] = $this->form->getThreeDsForm()->getCavv();

        }

        return $this->api->sendRequest($post_values);

    }

    function getSandboxCredentials() {
        return [
            'url' => 'https://test.authorize.net/gateway/transact.dll',
            'username' => get_option('wds_donation_authorizenet_merchant_id')
        ];
    }

    function getLiveCredentials() {
        return [
            'url' => 'https://secure.authorize.net/gateway/transact.dll',
            'username' => get_option('wds_donation_authorizenet_merchant_id')
        ];
    }

    public function getCredentials() {
        return $this->credentials;
    }
}