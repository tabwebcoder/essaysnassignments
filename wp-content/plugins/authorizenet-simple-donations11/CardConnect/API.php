<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 27/11/17
 * Time: 11:44 PM
 */

namespace Donations\CardConnect;

use CardConnect\CardConnectRestClient;
use Donations\Common\Classes\PaymentResponseObject;

class API {

    function sendRequest($params) {
        $method         = $params['method'];
        $request        = $params['request'];
        $credentials    = $params['credentials'];

        $client = new CardConnectRestClient(
            $credentials['url'],
            $credentials['username'],
            $credentials['password']
        );

        $result = $client->$method($request);

        return $this->createResponseObject($result);
    }

    function createResponseObject($result) {
        $status = $result['respstat'] == 'A' ?
            PaymentResponseObject::STATUS_SUCCESS :
            PaymentResponseObject::STATUS_FAILURE;

        $messages = array();
        $messages[] = $result['resptext'];

        return new PaymentResponseObject($status, $messages);
    }

}