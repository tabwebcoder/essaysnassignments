<?php

namespace Donations;

/*
Plugin Name: Simple Donations
Author: Aman Verma
Description: Accept donations simply with Authorize.net/Paycertify/Paycheck accounts. Easy to use and configure.
Version: 2.2
License: GPLv2 or later
*/

use Donations\Common\Utils\CommonSettings;

//add_shortcode('wds_donate', 'fn_wds_donate');
//add_action( 'init', 'wds_donation_type', 0 );
add_action('admin_menu', 'wds_create_menu');
//add_action( 'admin_init', 'donation_register_mysettings' );
//add_action( 'add_meta_boxes', 'donation_add_meta_box' );

function fn_wds_donate()
{

    $valid	  = true;
    $msg  	  = '';
    $donation  = false;
    $html 	  = '';

    if( isset( $_POST['wds_donate'])):
// $donor_phone = $_POST['donor_phone'];

        if( $_POST['donor_phone'] != ''){
            $donor_phone= $_POST['donor_phone'];
        }
        else{
            $valid = false;
            $msg.= 'Phone Number is required </br>';
        }

        if( $_POST['donor_firstname'] != ''){
            $donor_firstname = $_POST['donor_firstname'];
        }
        else{
            $valid = false;
            $msg.= 'First Name is required </br>';
        }

        if( $_POST['donor_lastname'] != ''){
            $donor_lastname = $_POST['donor_lastname'];
        }
        else{
            $valid = false;
            $msg.= 'Last Name is required </br>';
        }
        if( $_POST['donor_address'] != ''){
            $donor_address= $_POST['donor_address'];
        }
        else{
            $valid = false;
            $msg.= 'Address is required </br>';
        }
        if( $_POST['donor_city'] != ''){
            $donor_city= $_POST['donor_city'];
        }
        else{
            $valid = false;
            $msg.= 'City is required </br>';
        }
        if( $_POST['donor_zipcode'] != ''){
            $donor_zipcode= $_POST['donor_zipcode'];
        }
        else{
            $valid = false;
            $msg.= 'Zipcode is required </br>';
        }
        if( $_POST['donor_state'] != ''){
            $donor_state= $_POST['donor_state'];
        }
        else{
            $valid = false;
            $msg.= 'State is required </br>';
        }

        if( $_POST['donor_email'] != ''){
            $donor_email = $_POST['donor_email'];
            if( preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/" , $donor_email)){}
            else{
                $valid = false;
                $msg.= 'Invalid email format </br>';
            }
        }
        else{
            $valid = false;
            $msg.= 'E-mail is required </br>';
        }

        $donation_amount = 39.99;


        if( $_POST['donor_account_number'] != ''){
            $donor_account_number = $_POST['donor_account_number'];
            if(!is_numeric($donor_account_number)){
                $valid = false;
                $msg.= 'Please enter valid Account Number</br>';
            }
        }
        else{
            $valid = false;
            $msg.= 'Account Number is required </br>';
        }

        if( $_POST['donor_transit_number'] != ''){
            $donor_transit_number = $_POST['donor_transit_number'];
            if(!is_numeric($donor_transit_number)){
                $valid = false;
                $msg.= 'Please enter valid Transit Number</br>';
            }
        }
        else{
            $valid = false;
            $msg.= 'Transit Number is required </br>';
        }
        if( $_POST['donor_check_number'] != ''){
            $donor_check_number = $_POST['donor_check_number'];
            if(!is_numeric($donor_check_number)){
                $valid = false;
                $msg.= 'Please enter valid Check Number</br>';
            }
        }
        else{
            $valid = false;
            $msg.= 'Check Number is required </br>';
        }


        if( $valid ){
            $donor_firstname;
            $donor_lastname;
            $donor_address;
            $donor_zipcode;
            $donor_state;
            $donor_email;
            $donation_amount;
            $donor_account_number;
            $donor_transit_number;
            //$donor_cvv;
            //$donor_card_expiry;
            $auth_merchant_id        = get_option('wds_donation_merchant_id');
            //$auth_transaction_key = get_option('wds_donation_transaction_key');
            $auth_mode            = get_option('wds_donation_mode');
            //$flag_admin 		  = get_option('wds_admin_notification');
            //$flag_donor 		  = get_option('wds_donor_notification');
            $processor_description = get_option('wds_processor_description');

            if( $auth_mode == "live" ){
                $post_url = "https://ws.paysoftintern.com/service.asmx?WSDL";
            }
            else{
                $post_url = "https://ws.paysoftintern.com/service.asmx?WSDL";
                $auth_merchant_id = "888888";
            }

            $post_values = array(
                'strStoreID' => trim($auth_merchant_id),
                'strTransitNum' => trim($donor_transit_number),
                'strAccount' => trim($donor_account_number),
                'decAmount' => trim($donation_amount),
                'strReference' => trim($donor_check_number),
                'strTransID' => '2222',
                'strName' => trim($donor_firstname),
                'strLastName' => trim($donor_lastname),
                'strAddress1' => trim($donor_address),
                'strAddress2' => '',
                'strCity' => trim($donor_city),
                'strState' => trim($donor_state),
                'strZip' => trim($donor_zipcode),
                'strTelephone' => trim($donor_phone)
            );

            $client = null;
            $response = null;
            try {
                $client = new SoapClient($post_url,
                    array(
                        'trace' =>true,
                        'connection_timeout' => 500000,
                        'cache_wsdl' => WSDL_CACHE_BOTH,
                        'keep_alive' => false,
                    )
                );
                $response = $client->__soapCall("SubmitCheck", array($post_values));
            }
            catch (Exception $e) {
                $msg.= 'Unable to receive payment at the moment.</br>';
                $msg.= $e->getMessage() . '.</br>';
            }
            $result = $response->SubmitCheckResult;
            $resultObject = simplexml_load_string($result);


            if($resultObject->type == "A") {

                $post = array(
                    'post_type'    => 'wds_donation',
                    'post_title'   => 'Donation - '. sanitize_text_field($_POST['donor_firstname']). ' '. sanitize_text_field($_POST['donor_lastname']),
                    'post_status'  => 'publish',
                    'post_author'  => 1,
                );
                $post_id = wp_insert_post( $post );

                add_post_meta($post_id, 'donor_firstname', sanitize_text_field($_POST['donor_firstname']), true);
                add_post_meta($post_id, 'donor_lastname', sanitize_text_field($_POST['donor_lastname']), true);
                add_post_meta($post_id, 'donor_email', sanitize_text_field($_POST['donor_email']), true);
                add_post_meta($post_id, 'donation_amount', sanitize_text_field($_POST['donation_amount']), true);
                add_post_meta($post_id, 'transaction_id', sanitize_text_field($transaction_id), true);
                add_post_meta($post_id, 'last_4', sanitize_text_field($last_4), true);
                add_post_meta($post_id, 'approval_code', sanitize_text_field($approval_code), true);


                add_post_meta($post_id, 'donor_address', sanitize_text_field($_POST['donor_address']), true);

                add_post_meta($post_id, 'donor_zip', sanitize_text_field($_POST['donor_zip']), true);

                add_post_meta($post_id, 'donor_state', sanitize_text_field($_POST['donor_state']), true);
                add_post_meta($post_id, 'donor_phone', sanitize_text_field($_POST['donor_phone']), true);

                $donation = true;


//email

                $to = $_POST['donor_email'];
                $subject = "Registration Confirmation for US Postal Job";
//$headers = array('Content-Type: text/html; charset=UTF-8');

                $headers = array('Content-Type: text/html; charset=UTF-8','From: US Job Help Center <no-reply@postalhiringservice.com');
                $content = '<div style="width: 700px;display: block;margin:0 auto;font-family: seoge ui; " >
		<h3 style="font-size: 22px;">Registration Confirmation for US Postal Job</h1>
		<h3 style="color: red;">Please read this email carefully and completely, before proceeding.  It will save you time and make this easier.  </h3>

		<p style="font-size: 17px;line-height: 22px;">Congratulations.  You have successfully registered.</p>
		<p style="font-size: 17px;line-height: 22px;">The hiring process with the US Postal Service is the same for all entry level positions, which include Window Clerk, Mail Carrier, Mail Handler, Mail Processor and Rural Carrier Associate.  </p>

		<p style="font-size: 17px;line-height: 22px;">You will apply online, take an exam, and have an interview.  </p>

		<p style="font-size: 17px;line-height: 22px;">Typically, it takes about 2 weeks to get hired.</p>

		<p style="font-size: 17px;line-height: 22px;">Everything you need is made available to you on the “Resources” page of our website, which you can access by clicking on  <a rel="nofollow" href="http://www.usjobshelpcenter.com/resources/" target="_blank" >“Access My Resource”</a> link here, or at the bottom of this email:   </p>


		<h2 style="text-align: center;text-decoration: underline;color: #4472c4;">Access My Resources</h2>
		
		<h3>Before you get started, there are 3 main things you need to know. </h3>
		
		<h4>#1 – LIVE ASSISTANCE IS AVAILABLE TO HELP YOU</h4>

		<p style="font-size: 17px;line-height: 22px;">We are here for you.  All questions are welcome.  Just call us at <strong style="color: #70ad47;font-size: 26px;">(954) 900-1435</strong>, or start a chat on the   <a rel="nofollow" href="http://www.usjobshelpcenter.com/resources/" target="_blank" >“Resources”</a> page.    </p>

		<p style="font-size: 17px;line-height: 22px;">Live Postal Job Placement Specialists are available Monday thru Friday, 9:00 AM to 9:00 PM EST. </p>


		<h4>#2 - JOB AVAILABILITY CAN BE CONFUSING</h4>

		<p style="font-size: 17px;line-height: 22px;">Some job openings are announced to the public, but not all.  Many job openings are only announced to candidates who pass the exam.</p>    


		<p style="font-size: 17px;line-height: 22px;">In order to take the exam, simply apply for the closest job to you.  Don’t worry if it’s not the job you really want.  </p>

		<p style="font-size: 17px;line-height: 22px;">After you pass the exam, you will be able to see ALL the jobs available in your area.   </p>




		<h4>#3 – THE EXAM IS NOT HARD, BUT IT IS TIMED</h4>

		<p style="font-size: 17px;line-height: 22px;">The exam is the most important part of the hiring process.   You will take it at an independent testing facility in your area.  </p>    


		<p style="font-size: 17px;line-height: 22px;">The questions on the exam are not difficult.  The issue is time. </p>

		<p style="font-size: 17px;line-height: 22px;">So, be sure to practice for the exam on our web site first, so you know how fast you need to go.   </p>

		<p style="font-size: 17px;line-height: 22px;">Most candidates do not practice for it, and only 20% pass it, but 90% of those who practice ahead of time on our site pass it.  </p>

		<p style="font-size: 17px;line-height: 22px;">Remember, to get hired, you must first pass the exam.</p>

		<p style="font-size: 17px;line-height: 22px;">You are on the right track, and perhaps only 2 weeks away from a fantastic new career.  </p>

		<p style="font-size: 17px;line-height: 22px;">Congratulations once again.  </p>

		<p style="text-align: center;margin-top: 50px;font-size: 17px;">To continue, click this link below to…</p>
		<h2 style="text-align: center;text-decoration: underline;color: #4472c4;margin-top: -11px;font-size: 34px;"> <a rel="nofollow" href="http://www.usjobshelpcenter.com/resources/" target="_blank" >Access My Resources</a></h2>

		<h1 style="text-align: center;color: #70ad47;font-size: 35px;">Live Phone Support (954) 900-1435</h1>

		<p style="text-align: center;margin-top: 0px;font-size: 17px;">Monday thru Friday, 9:00 AM to 9:00 PM EST</p>

</div>';

                $status = wp_mail($to, $subject, $content, $headers);
//email


            }

            else if($resultObject->type == "R"){
                $msg .= $resultObject->message;

            }
            else{
                $msg .= "Unable to process your request at the moment. Please contact support";
            }


        }
        else{

        }
    endif;

    if($msg != ''){
        $html .= '<div style="border: dotted 3px #F60109;padding: 5px; padding-left: 20px; margin-bottom: 20px;">
				'.$msg.'
				</div>';
    }

    if($donation){


        $html .= '<script>window.location.replace("http://postaljobresources.net/success/");</script>';


    }
    else{

        $html .= '<form method="post" class="step_4">
				<div class="form-group"><label style="width:50%">First Name</label> <input type="text"  style="width:50%" name="donor_firstname" id="donor_firstname"> </div>
				<div class="form-group"><label style="width:50%">Last Name</label> <input type="text"  style="width:50%" name="donor_lastname" id="donor_lastname"> </div>
				<div class="form-group"><label style="width:50%">E-mail</label> <input type="text" style="width:50%" name="donor_email" id="donor_email"> </div>

<div class="form-group"><label style="width:50%">Address</label> <input type="text"  style="width:50%" name="donor_address" id="donor_address"> </div>
<div class="form-group"><label style="width:50%">Phone</label> <input type="text" maxlength="12" style="width:50%" name="donor_phone" id="donor_phone" > </div>
<div class="form-group"><label style="width:50%">City</label> <input type="text"  style="width:50%" name="donor_city" id="donor_city"> </div>
<div class="form-group"><label style="width:50%">State</label> <input type="text"  style="width:50%" name="donor_state" id="donor_state"> </div>
<div class="form-group"><label style="width:50%">Zipcode</label> <input type="text"  style="width:50%" name="donor_zipcode" id="donor_zipcode"> </div>
		
<h3>Payment Details</h3>
<div class="card-icon" style="margin-bottom:20px;">
        <h4>
            Secure SSL Payment &nbsp; <img class="secure" title="" alt="Secure" src="http://www.postofficejobcenter.com/images/icon_secure.png">
        </h4>
    </div>

				<div class="form-group paymf1"><label style="width:50%">Account Number</label> <input type="text" maxlength="25" style="width:50%" name="donor_account_number" id="donor_account_number"> </div>
				<div class="form-group paymf1"><label style="width:50%">Transit Number</label> <input type="text" maxlength="25" style="width:50%" name="donor_transit_number" id="donor_transit_number"> </div>
				<div class="form-group paymf1"><label style="width:50%">Check Number</label> <input type="text" maxlength="25" style="width:50%" name="donor_check_number" id="donor_check_number"> </div>

 </div>
				<div class="form-group btn-all"> <input type="submit" style="    padding: 10px !important; font-size: 20px !important;" class="wpcf7-form-control wpcf7-submit orange-btn vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-color-danger" name="wds_donate" value="Submit"> </div>
			</form>';

    }

    return $html;
}

function wds_donation_type() {

    $labels = array(
        'name'                => _x( 'Donations', 'Post Type General Name', 'wds_donation' ),
        'singular_name'       => _x( 'Donation', 'Post Type Singular Name', 'wds_donation' ),
        'menu_name'           => __( 'Donation ', 'wds_donation' ),
        'parent_item_colon'   => __( 'Parent Donation', 'wds_donation' ),
        'all_items'           => __( 'All Donation', 'wds_donation' ),
        'view_item'           => __( 'View Donation', 'wds_donation' ),
        'add_new_item'        => __( 'Add New Donation', 'wds_donation' ),
        'add_new'             => __( 'Add New', 'wds_donation' ),
        'edit_item'           => __( 'Edit Donation', 'wds_donation' ),
        'update_item'         => __( 'Update Donation', 'wds_donation' ),
        'search_items'        => __( 'Search Donation', 'wds_donation' ),
        'not_found'           => __( 'Not found', 'wds_donation' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'wds_donation' ),
    );
    $args = array(
        'label'               => __( 'wds_donation', 'wds_donation' ),
        'description'         => __( 'list of donations', 'wds_donation' ),
        'labels'              => $labels,
        'supports'            => array('title', 'custom-fields' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'wds_donation', $args );

}

function wds_create_menu() {

//    $menu = new AdminMenu();
//    $menu->addMenu();
    add_menu_page('Donation Settings', 'Donation Settings', 'administrator', 'donation-settings-page', 'donation_settings_page');
}

function donation_register_mysettings() {

    $commonSettings = new CommonSettings();
    $authorizeNetSettings = new \Donations\AuthorizeNet\Classes\AuthorizeNetSettings();
    $paycheckSettings = new \Donations\Paycheck\Classes\PayCheckSettings();

    $commonSettings->registerSettings();
    $authorizeNetSettings->registerSettings();
    $paycheckSettings->registerSettings();

}

function donation_settings_page() {

    $commonSettings = new CommonSettings();
    $authorizeNetSettings = new \Donations\AuthorizeNet\Classes\AuthorizeNetSettings();
    $payCheckSettings = new \Donations\Paycheck\Classes\Settings();

    $settings = "";
    $settings .= $authorizeNetSettings->getSettingsPage();
    $settings .= $payCheckSettings->getSettingsPage();

    echo $commonSettings->getSettingsPage();
}

function donation_add_meta_box()
{
    $screens = array( 'wds_donation' );

    foreach ( $screens as $screen ) {

        add_meta_box(  'myplugin_sectionid', __( 'Donation Information', 'myplugin_textdomain' ),'donation_meta_box_callback', $screen, 'normal','high' );
    }
}


function donation_meta_box_callback($post)
{

    ?>

    <ul>
        <li>
            <div class="html-label">First Name</div>

            <div class="html-field form-group" ><input type="text" name="donor_firstname" id=“donor_firstname" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'donor_firstname', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">Last Name</div>

            <div class="html-field form-group"><input type="text" name="donor_lastname" id="donor_last_name" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'donor_lastname', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">E-mail</div>

            <div class="html-field form-group"><input type="text" name="donor_email" id="donor_email" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'donor_email', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">Amount (USD)</div>

            <div class="html-field form-group"><input type="text" name="donation_amount" id="donation_amount" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'donation_amount', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">Transaction ID</div>

            <div class="html-field form-group"><input type="text" name="transaction_id" id="transaction_id" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'transaction_id', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">Last 4</div>

            <div class="html-field form-group"><input type="text" name="last_4" id="last_4" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'last_4', true ));?>"></div>
        </li>

        <li>
            <div class="html-label">ApprovalCode</div>

            <div class="html-field form-group"><input type="text" name="approval_code" id="approval_code" disabled value="<?php echo esc_attr(get_post_meta( $post->ID, 'approval_code', true ));?>"></div>
        </li>
    </ul>


<?php

}