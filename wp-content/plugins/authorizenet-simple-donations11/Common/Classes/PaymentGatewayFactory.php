<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 26/08/17
 * Time: 8:37 PM
 */

namespace Donations\Common\Classes;


use Donations\Authorizenet\AuthorizeNet;
use Donations\CardConnect\CardConnect;
use Donations\Common\Interfaces\IPayment;
use Donations\Paycertify\PayCertify;
use Donations\Paycheck\PayCheck;
use Donations\EPin\EPin;

class PaymentGatewayFactory {
    var $paymentMethod;

    function getPaymentGateway(PaymentForm $form, $environment, $ccPaymentProcessor) {
        //get type of payment gateway e.g: credit card or checking account
        if($form->getDonorPaymentMethod() == PaymentForm::GATEWAY_CHECKINGACCOUNT) {
            return new PayCheck($form, $environment);
        }
        else if($form->getDonorPaymentMethod() == PaymentForm::GATEWAY_CREDITCARD) {
            if($ccPaymentProcessor == IPayment::PAYMENT_GATEWAY_AUTHORIZENET)
                return new AuthorizeNet($form, $environment);
            else if($ccPaymentProcessor == IPayment::PAYMENT_GATEWAY_PAYCERTIFY)
                return new PayCertify($form, $environment);
            else if($ccPaymentProcessor == IPayment::PAYMENT_GATEWAY_EPIN)
                return new EPin($form, $environment);
            else if($ccPaymentProcessor == IPayment::PAYMENT_GATEWAY_CARDCONNECT)
                return new CardConnect($form, $environment);

        }
    }
}