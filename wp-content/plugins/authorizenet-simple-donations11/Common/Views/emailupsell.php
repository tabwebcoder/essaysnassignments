<style type="text/css">@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700');

@import url('https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700');

@font-face {
    font-family:'Open Sans', sans-serif;
    src: url(OpenSans-Regular.ttf);
}
@font-face {
    font-family:'Raleway', sans-serif;
    src: url(Raleway-Regular.ttf);
}
</style>
<table align="center" border="0" class="container-table" line-height:="" style="background-image: url(http://job-usps.com/media/com_acymailing/upload/missionbg.jpgs); background-size: cover; display:block; margin:0 auto;" width="85%">
	<tbody>
		<tr style="line-height: 0px;">
			<td bgcolor="#202F64" style="padding: 30px 20px;border-bottom: 5px solid #ff0023;"><img src="http://job-usps.com/images/logo-footer.png" style="
" /></td>
		</tr>
		<tr>
			<td height="61" style="font-size: 30px; text-align: center; color: #1f4394; font-family: 'Raleway', sans-serif; margin: 0px 0px; margin-top: 20px; font-family: 'Raleway', sans-serif; line-height: 34px; font-weight: bold; text-align:center;">
			<p>&ldquo;Registration&rdquo; Confirmation for US Postal Job</p>
			</td>
		</tr>
		<tr>
			<td style="color: #70ad47;font-size: 18px;text-align: center;font-weight: 700;font-family: 'Raleway', sans-serif;">Congratulations. You have successfully registered. No need to pay again.</td>
		</tr>
		<tr style="padding: 35px 0px;display: block;">
			<td>
			<p style="font-weight: 500;font-size: 18px;line-height: 24px;color: #333;font-family:'Open Sans', sans-serif;padding: 0px 20px;">The hiring process with the US Postal Service is the same for all entry level positions, which include Window Clerk, Mail Carrier, Mail Handler, Mail Processor and Rural Carrier Associate.</p>

			<p style="font-weight: 500;font-size: 18px;line-height: 24px;color: #333;font-family:'Open Sans', sans-serif;padding: 0px 20px;">You will apply online, take an exam, and have an interview. Typically, it takes about 2 weeks to get hired.</p>

			<p style="font-weight: 500;font-size: 18px;line-height: 24px;color: #333;font-family:'Open Sans', sans-serif;padding: 0px 20px;">Everything you need is made available to you on the &ldquo;Access My Resource&rdquo; page of our website, which you can access by clicking on this button below.</p>
			</td>
		</tr>
		<tr align="center" style="padding: 10px 0px; display:block;">
			<td style="color: #70ad47; font-size: 17px; text-align: center; font-size: 20px; font-weight: bold;"><a clicktracking="off" href="http://uspsjob.org" style="
       background-color: #ff0023;
    padding: 8px 10px;
    border-radius: 30px;
    font-size: 22px;
    text-decoration: none;
    color: #fff;
    font-weight: 500;
    display: block;
    margin: 0px auto;
    width: 240px;
    text-align: center;
" target="_blank">Access My Resources</a></td>
		</tr>
		<tr>
			<td height="61" style="font-size: 33px; text-align: center; color: #1f4394; font-family: 'Raleway', sans-serif; margin: 0px 0px; margin-top: -9px; font-family: 'Raleway', sans-serif; line-height: 34px; font-weight: bold; text-align:center;"><br />
			&ldquo;Ace Your Interview&rdquo; Course</td>
		</tr>
		<tr>
			<td style="padding-top: 10px;color: #70ad47;font-size: 18px;text-align: center;font-weight: 700;font-family: 'Raleway', sans-serif;">In addition, you secured the &ldquo;Ace Your Interview&rdquo; Course.</td>
		</tr>
		<tr style="padding: 35px 0px;display: block;">
			<td>
			<p style="font-weight: 500;font-size: 18px;line-height: 24px;color: #333;font-family:'Open Sans', sans-serif;padding: 0px 20px;">Your ability to sell yourself with confidence starts right now! Here is the link to the value-packed online course called &ldquo;Ace Your Interview&rdquo;:</p>
			</td>
		</tr>
		<tr align="center" style="padding: 10px 0px; display:block;">
			<td style="color: #70ad47; font-size: 17px; text-align: center; font-size: 20px; font-weight: bold;"><a clicktracking="off" href="https://erinmillerinc.mykajabi.com/p/usps-registration-page-for-access-to-training" style="
       background-color: #ff0023;
    padding: 8px 10px;
    border-radius: 30px;
    font-size: 22px;
    text-decoration: none;
    color: #fff;
    font-weight: 500;
    display: block;
    margin: 0px auto;
    width: 240px;
    text-align: center;
" target="_blank">Access My Courses</a></td>
		</tr>
		<tr>
			<td>
			<p style="font-weight: 500;font-size: 18px;line-height: 24px;color: #333;font-family:'Open Sans', sans-serif;padding: 0px 20px;    margin-top: 33px;">In this resource, you will learn how to master ANY interview, in ANY market, at ANY time. Get started now!</p>
			</td>
		</tr>
	</tbody>
</table>