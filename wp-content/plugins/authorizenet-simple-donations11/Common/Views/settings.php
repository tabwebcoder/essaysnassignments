<div class="wrap">
    <h1>Donation Settings</h1>

    <?php
    $pluginDir = ABSPATH . 'wp-content/plugins/authorizenet-simple-donations/';

    use \Donations\Common\Interfaces\IPayment;

    if (isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true'):
        echo '<div id="setting-error-settings_updated" class="updated settings-error">
<p><strong>Settings saved.</strong></p></div>';
    endif;
    ?>

    <form method="post" action="options.php">
        <?php settings_fields('wds-settings-group'); ?>
        <?php do_settings_sections('wds-settings-group'); ?>
        <h2>General Settings</h2>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Payment Processing Methods</th>
                <td>
                    <input type="checkbox" name="wds_credit_card_enabled" value="1" <?php checked( 1 == get_option('wds_credit_card_enabled') ); ?>> Credit Card
                    <input type="checkbox" name="wds_checking_account_enabled" value="1" <?php checked( 1 == get_option('wds_checking_account_enabled')); ?>> Checking Account
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Mode(Live/Test Sandbox)</th>
                <td>
                    <select name="wds_donation_mode" />
                    <option value="<?php echo IPayment::ENVIRONMENT_LIVE;?>" <?php if( get_option('wds_donation_mode') == IPayment::ENVIRONMENT_LIVE ): echo 'selected'; endif;?> >Live</option>
                    <option value="<?php echo IPayment::ENVIRONMENT_SANDBOX;?>" <?php if( get_option('wds_donation_mode') == IPayment::ENVIRONMENT_SANDBOX ): echo 'selected'; endif;?> >Test/Sandbox</option>
                    </select>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Credit Card Processor</th>
                <td>
                    <select name="wds_payment_gateway" />
                    <option value="<?php echo IPayment::PAYMENT_GATEWAY_AUTHORIZENET ?>" <?php if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_AUTHORIZENET ): echo 'selected'; endif;?> >Authorize.net</option>
                    <option value="<?php echo IPayment::PAYMENT_GATEWAY_PAYCERTIFY ?>" <?php if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_PAYCERTIFY ): echo 'selected'; endif;?> >Pay Certify</option>
                    <option value="<?php echo IPayment::PAYMENT_GATEWAY_EPIN ?>" <?php if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_EPIN ): echo 'selected'; endif;?> >Gift Card (Epin) </option>
                    <option value="<?php echo IPayment::PAYMENT_GATEWAY_CARDCONNECT ?>" <?php if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_CARDCONNECT ): echo 'selected'; endif;?> >CardConnect </option>
                    </select>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">Thank You Message</th>
                <td><input type="text" style="width:50%" name="wds_thankyou_message" value="<?php echo get_option('wds_thankyou_message'); ?>" placeholder="Thank you message visible to Donor after donation" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Processor Description</th>
                <td><input type="text" style="width:50%" name="wds_processor_description" value="<?php echo get_option('wds_processor_description'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Test Emails <span style="font-size: 9px;">(comma seperated)</span></th>
                <td><input type="text" style="width:50%" name="wds_test_emails" value="<?php echo get_option('wds_test_emails'); ?>" /></td>
            </tr>
        </table>
        <h2>3ds Settings</h2>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">3ds Enabled</th>
                <td>
                    <input type="checkbox" name="wds_donation_3ds_enabled" value="1" <?php checked( 1 == get_option('wds_donation_3ds_enabled') ); ?> />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">3ds Provider</th>
                <td>
                    <select name="wds_donation_3ds_provider" />
                    <option value="cardinal" <?php if( get_option('wds_donation_3ds_provider') == "cardinal" ): echo 'selected'; endif;?> >Cardinal Commerce</option>
                    </select>
                </td>
            </tr>
        </table>
        <h2>SMS Settings</h2>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Website URL</th>
                <td><input type="text" style="width:50%" name="wds_donation_sms_website" value="<?php echo get_option('wds_donation_sms_website'); ?>" placeholder="Website URL" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Support Contact</th>
                <td><input type="text" style="width:50%" name="wds_donation_sms_support_number" value="<?php echo get_option('wds_donation_sms_support_number'); ?>" placeholder="Support Contact Number" /></td>
            </tr>
        </table>
<h2>Upsell Product Settings</h2>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Upsell Product: </th>
                <td>
                <select name="wds_settings_upsell_enabled" />
                    <option value="1" <?php if( get_option('wds_settings_upsell_enabled') == "1" ): echo 'selected'; endif;?> >Yes</option>
                    <option value="0" <?php if( get_option('wds_settings_upsell_enabled') == "0" ): echo 'selected'; endif;?> >No</option>
                </select>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Upsell Amount: </th>
                <td><input type="text" style="width:50%" name="wds_settings_upsell_amount" value="<?php echo get_option('wds_settings_upsell_amount'); ?>" placeholder="Upsell product price" /></td>
            </tr>
        </table>

        <?php
        $authorizeNetSettings = $pluginDir . 'Authorizenet/Views/settings.php';
        $payCheckSettings = $pluginDir . 'Paycheck/Views/settings.php';
        $payCertifySettings = $pluginDir . 'Paycertify/Views/settings.php';
        $ePinSettings = $pluginDir . 'EPin/Views/settings.php';
        $cardConnectSettings = $pluginDir . 'CardConnect/Views/settings.php';

        include $authorizeNetSettings;
        include $payCheckSettings;
        include $payCertifySettings;
        include $ePinSettings;
        include $cardConnectSettings;

        ?>

        <?php submit_button(); ?>

    </form>
    <p style="font-weight:bold">Use shortcode [wds_donate]</p>
</div>
