<?php
use \Donations\Common\Utils\Helper;
use \Donations\Common\Classes\PaymentForm;
use \Donations\Common\Interfaces\IPayment;

$pluginDir = ABSPATH . 'wp-content/plugins/authorizenet-simple-donations/';

if($messages != ''){
    echo '<div style="border: dotted 3px #F60109;padding: 5px; padding-left: 20px; margin-bottom: 20px;">
				' . $messages . '
				</div>';
}
if(isset($_GET['msg'])) {
    echo '<div style="border: dotted 3px #F60109;padding: 5px; padding-left: 20px; margin-bottom: 20px;">
				' . $_GET['msg'] . '
				</div>';
}


?>

<form method="post" class="step_4">
    <div class="form-group"><label style="width:50%">First Name</label> <input type="text"  style="width:50%" name="donor_firstname" value="<?php echo Helper::form_value("donor_firstname");?>" id="donor_firstname"> </div>
    <div class="form-group"><label style="width:50%">Last Name</label> <input type="text"  style="width:50%" name="donor_lastname" value="<?php echo Helper::form_value("donor_lastname");?>" id="donor_lastname"> </div>
    <div class="form-group"><label style="width:50%">Phone</label> <input type="text" maxlength="12" class="marg2" style="width:50%" name="donor_phone" placeholder="(123) 456-7890" value="<?php echo Helper::form_value("donor_phone");?>" id="donor_phone" ><p class="para1 mypra">You will receive vital communications via text to this number </p> </div>
    
    <div class="form-group"><label style="width:50%">Re-Enter Phone</label> <input placeholder="(123) 456-7890" type="text" maxlength="12" class="marg2"style="width:50%" name="donor_phone2"  value="<?php echo Helper::form_value("donor_phone2");?>" id="donor_phone2" > <div id="phone_error"></div></div>




    <div class="form-group"><label style="width:50%" class="email-text">eMail</label> <input type="email" style="width:50%" name="donor_email" value="<?php echo Helper::form_value("donor_email");?>" id="donor_email"><p class="para1">You will receive an email with a link to access your account</p>  </div>
    
    <div class="form-group"><label style="width:50%">Re-Enter eMail</label> <input type="email" style="width:50%; margin-top: -11px;" name="donor_email2" value="<?php echo Helper::form_value("donor_email2");?>" id="donor_email2"><div id="myerror"></div>  </div>

    <div class="form-group"><label style="width:50%" class="marg">Address</label> <input type="text" class="marg" style="width:50%" name="donor_address" value="<?php echo Helper::form_value("donor_address");?>" id="donor_address"> </div>
    
    
    
    
    <div class="form-group"><label style="width:50%">City</label> <input type="text"  style="width:50%" name="donor_city" value="<?php echo Helper::form_value("donor_city");?>" id="donor_city"> </div>
    <div class="form-group expire-card">
        <label style="width:50%">State</label>
        <select style="width:50%; float: right;" name="donor_state" value="<?php echo Helper::form_value("donor_state");?>" id="donor_state">
            <?php foreach (Helper::getStates() as $initials => $name): ?>
                <option <?php echo Helper::selected_value('donor_state', $initials) ;?> value="<?php echo $initials;?>"><?php echo $name;?></option>
            <?php endforeach;?>
        </select>
    </div>
    <div class="form-group"><label style="width:50%">Zip Code (5 digit)</label> <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
    type = "number"
    maxlength = "5" style="width:50%" name="donor_zipcode" value="<?php echo Helper::form_value("donor_zipcode");?>" id="donor_zipcode"> </div>

    <h3>Payment Details</h3>
    <div class="card-icon" style="margin-bottom:20px;">
        <h4>
            Secure SSL Payment &nbsp; <img class="secure" title="" alt="Secure" src="https://postofficehiring.com/wp-content/uploads/2018/05/icon_secure.png">
        </h4>
    </div>




    <?php if(get_option('wds_credit_card_enabled') == "1") { ?>
        <div class="form-group"  style="display:none;">
            <input  id="radio-credit-card" type="radio" name="donor_payment_method" onclick="selectCreditCard()" <?php echo Helper::isPaymentMethodSelected(PaymentForm::GATEWAY_CREDITCARD);?> value="<?php echo PaymentForm::GATEWAY_CREDITCARD ?>"/> Credit Card
        </div>
    <?php } ?>



    <?php if(get_option('wds_checking_account_enabled') == "1") { ?>
        <div class="form-group">
            <input id="radio-checking-account" type="radio" name="donor_payment_method" onclick="selectCheckingAccount()" <?php echo Helper::isPaymentMethodSelected(PaymentForm::GATEWAY_CHECKINGACCOUNT);?> value="<?php echo PaymentForm::GATEWAY_CHECKINGACCOUNT?>"/> Checking Account
        </div>
    <?php } ?>





    <?php if(get_option('wds_credit_card_enabled') == "1") { ?>
        <div id="credit-card-form" style="display: none">
            <?php
            if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_AUTHORIZENET ) {
                $authorizeNetForm = $pluginDir . 'Authorizenet/Views/form.php';
                include $authorizeNetForm;
            }
            else if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_PAYCERTIFY ) {
                $payCertifyForm = $pluginDir . 'Paycertify/Views/form.php';
                include $payCertifyForm;
            }
            else if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_EPIN ) {
                $ePinForm = $pluginDir . 'EPin/Views/form.php';
                include $ePinForm;
            }
            else if( get_option('wds_payment_gateway') == IPayment::PAYMENT_GATEWAY_CARDCONNECT ) {
                $cardConnectForm = $pluginDir . 'CardConnect/Views/form.php';
                include $cardConnectForm;
            }
            ?>
        </div>
    <?php } ?>


    <?php if(get_option('wds_checking_account_enabled') == "1") { ?>
        <div id="paycheck-form" style="display: none;">
            <?php
            $payCheckForm = $pluginDir . 'Paycheck/Views/form.php';
            include $payCheckForm;
            ?>
        </div>
    <?php }
    if(get_option('wds_donation_3ds_enabled') == "1") {
        echo "<input type='hidden' name='3ds' value='1' />";
    }
    ?>
<p  class="nominal-text mr" style="width: 50%;    text-align: center;">By clicking "Submit" I agree to the <a href="/terms-conditions/">Terms and Conditions </a></p>

    <div class="form-group btn-all" style="    margin-bottom: 20px !important; ">
        <input type="submit"
               style="padding: 10px !important; font-size: 20px !important;"
               class="wpcf7-form-control wpcf7-submit orange-btn vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-flat vc_btn3-color-danger"
               name="wds_donate"
               id="step-4-submit"
               value="Submit">
    </div>
        <p  class="nominal-text" style="width: 50%;;    text-align: center;">A nominal and refundable processing fee of $7.95 will be added to your $39 refundable registration deposit</p>


</form>
 
<script>
    function selectCreditCard() {
        document.getElementById('credit-card-form').style.display = 'block';
        // hide the lorem ipsum text
        document.getElementById('paycheck-form').style.display = 'none';
    }

    function selectCheckingAccount() {
        document.getElementById('paycheck-form').style.display = 'block';
        // hide the lorem ipsum text
        document.getElementById('credit-card-form').style.display = 'none';
    }

    function displaySelectedPaymentMethod() {
        if(document.getElementsByName('donor_payment_method')[0].checked) {
            selectCreditCard();
        }
        else {
            selectCheckingAccount();
        }
    };

    function defaultPaymentMethod(method) {
        if(method == "checking-account") {
            document.getElementById('radio-checking-account').click();
        }
        else if (method == "credit-card") {
            document.getElementById('radio-credit-card').click();
        }
    }

    window.onload = displaySelectedPaymentMethod;
    defaultPaymentMethod('credit-card');
</script>
<script type="text/javascript">

$("#donor_email2").blur(function() {
        if($("#donor_email").val() != $("#donor_email2").val())
{
          $("#myerror").html("This entry should match the first one entered.");
$("#myerror").show();
return false;
} 
else{
$("#myerror").hide();
}

 });

$("#donor_phone2").blur(function() {
        if($("#donor_phone").val() != $("#donor_phone2").val())
{
          $("#phone_error").html("This entry should match the first one entered.");
$("#phone_error").show();
return false;
} 
else{
$("#phone_error").hide();
}

 });

</script>

