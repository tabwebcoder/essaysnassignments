<style scoped="" type="text/css">@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700');

@import url('https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700');

@font-face {
    font-family:'Open Sans', sans-serif;
    src: url(OpenSans-Regular.ttf);
}
@font-face {
    font-family:'Raleway', sans-serif;
    src: url(Raleway-Regular.ttf);
}

@media only screen and (max-width : 320px) {
    .head {
        width: 100% !important;
    }
    .head img {
        width: 100% !important;
        display: block;
        margin: 0 auto;
    }
    .head span {
        float: none !important;
        margin: 0 auto;
        display: block;
    }
    .mid { 
        width: 100% !important;
     }
     .footer {
        width: 100% !important;
     }
       a {
           text-align: center;
    		display: block !important;
    		width: 70% !important;
    		margin: 0 auto !important;
    		font-size: 26px !important;
   		 float: none !important;
        }
    p {
      	 text-align: center !important;
    	font-size: 20px !important;
   	 	line-height: 48px !important;
    	width: 300px;
    	display: block;
    }
    h1 {
        font-size:48px !important;
        line-height: 60px !important;
       	width: 300px;
    	display: block;
    }
    h4 {
        text-align: center !important;
       width: 300px;
    display: block;
    }
  .access {
        width: 200px !important;
    	margin: 0px 0px!important;
   	 font-size: 18px !important;
	  float: none !important;
  }
.myspan {
	 font-size: 26px !important;
	 line-height: 30px!important;
	 }
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<table align="center" border="0" class="mid" width="830px">
	<tbody style="padding:0px 20px;">
		<tr>
			<td colspan="3" style="background-color: #202f64;padding: 15px 20px;border-bottom: 5px solid #ff0023;"><img alt="" height="43" src="http://job-usps.com/media/com_acymailing/upload/logo_footer.png" />
			<p></p>
			<a href="tel:9549001435" style="padding: 5px 16px;border-radius: 30px;font-size: 25px;
    text-decoration: none;color: #fff;font-weight: 500;background-color: #FF0023;float: right;margin-top: -56px;
">(954) 900-1435</a></td>
		</tr>
		<tr>
			<td colspan="3" style="padding: 0px 20px; text-align: center;"><span class="myspan" style="font-size: 36px; text-align: center; color: #1f4394; font-family: 'Raleway', sans-serif; line-height: 42px; font-weight: bold;">Registration Confirmation for US Postal Job</span></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center;"><span style="font-size: 20px;line-height: 26px;color: #70ad47;font-weight: 600;text-align: center;margin-top: 20px;font-family: 'Raleway', sans-serif;margin-bottom: 0px;">Congratulations. You have successfully registered. No need to pay again.</span></td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center;"><span style="color: red;font-size: 14px;text-align: center;margin-top: 4px;">Please read this email carefully and completely, before proceeding. It will save you time and simplify the process.</span></td>
		</tr>
		<tr>
			<td colspan="3" style="padding: 0px 20px;">
			<p style="font-size: 14px;line-height: 22px;color: #333;margin-top: 32px;">The hiring process with the US Postal Service is the same for all entry level positions, which include Window Clerk, Mail Carrier, Mail Handler, Mail Processor and Rural Carrier Associate.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">You will apply online, take an exam, and have an interview.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">Typically, it takes about 2 weeks to get hired.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">Everything you need is made available to you on the &ldquo;Access My Resource&rdquo; page of our website, which you can access by clicking on</p>
			</td>
		</tr>
		<tr>
			<td colspan="3"><a class="access" href="http://job-postal.com/" rel="nofollow" style="
       background-color: #ff0023;
    padding: 8px 10px;
    border-radius: 30px;
    font-size: 22px;
    text-decoration: none;
    color: #fff;
    font-weight: 500;
    display: block;
    margin: 0 auto;
    width: 240px;
    text-align: center;
        margin-top: 22px;
" target="_blank">Access My Resources</a></td>
		</tr>
		<tr>
			<td colspan="3" height="38" style="text-align: center;"><br />
			<span style="color: #1f4394; text-align: center; font-family: 'Raleway', sans-serif; font-size: 20px; line-height: 25px; font-weight: bold;">Before you get started, there are 3 important things you should know.</span></td>
		</tr>
		<tr>
			<td colspan="3" height="32" style="padding: 0px 20px;"><span style="color: #ff0023; font-family: 'Raleway', sans-serif; font-weight: bold;">#1 &ndash; LIVE ASSISTANCE IS AVAILABLE TO HELP YOU</span></td>
		</tr>
		<tr>
			<td colspan="3" style="padding: 0px 20px;">
			<p style="font-size: 14px;line-height: 22px;color: #333;">We are here to help you get hired. All questions are welcome. Just call us or start a chat on the &ldquo;Resources&rdquo; page.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">Live Postal Job Placement Specialists are available Monday thru Friday, 9:00 AM to 9:00 PM EST.</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="padding: 0px 20px;">
			<h4 style="color: #ff0023; font-family: 'Raleway', sans-serif;">#2 - JOB AVAILABILITY CAN BE CONFUSING</h4>

			<p style="font-size: 14px;line-height: 22px;color: #333;">Some job openings are announced to the public, but not all. Other jobs, which are not announced to the public, are made available ONLY to candidates who have already passed the exam.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">In order to take the exam, simply apply for ANY of the entry level jobs in the United States. Don&#39;t worry if it&#39;s not the job you really want.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">After you pass the exam, you will see ALL available jobs publicly announced in your are, in addition to bening considered for jobs that may not be seen by the general public.</p>

			<h4 style="color: #ff0023; font-family: 'Raleway', sans-serif;">#3 &ndash; THE EXAM IS NOT HARD, BUT IT IS TIMED</h4>

			<p style="font-size: 14px;line-height: 22px;color: #333;">The exam is the most important part of the hiring process. You will take it at an independent testing facility in your area.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">The questions on the exam are not difficult. The issue is time.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">So, be sure access your Resources to practice for the exam FIRST, so you know how fast you need to move through the official exam.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">Most candidates do not feel the need to practice and only 20% pass the exam. But 90% of those who practice in advance with our Resources pass it.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">Remember, to get hired, you must first pass the exam.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">You are on the right track, and perhaps only 2 weeks away from a fantastic new career.</p>

			<p style="font-size: 14px;line-height: 22px;color: #333;">Congratulations once again.</p>
			</td>
		</tr>
		<tr>
		</tr>
		<tr style="background-image:url(http://job-usps.com/media/com_acymailing/upload/usps_bg.png)">
			<td colspan="3" style="padding: 0px 20px;">
			<p style="text-align: center;margin-top: 18px;font-size: 18px;color: #fff;">To continue, click this link below to&hellip;</p>

			<h2 style="text-align: center;text-decoration: underline;color: #fff;font-family: 'Raleway', sans-serif;margin-top: -11px;font-size: 32px;font-family: 'Raleway', sans-serif;"><a href="http://job-postal.com/" >Access My Resources</a></h2>

			<h5 style="text-align: center;color: #fff;font-size: 35px;margin-top: -4px;margin-bottom: 24px;">Live Phone Support</h5>
			<span style=" display: block;
    text-align: center;
    margin-bottom: 20px;"><a href="tel:9549001435" style="
    padding: 5px 22px;
    border-radius: 30px;
    font-size: 22px;
    text-decoration: none;
    color: #fff;
    font-weight: 500;
    background-color: #FF0023;
">(954) 900-1435</a></span>

			<p style="text-align: center;margin-top: 0px;font-size: 20px;color: #fff;">Monday thru Friday, 9:00 AM to 9:00 PM EST</p>
			</td>
		</tr>
	</tbody>
</table>
