<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 27/11/17
 * Time: 11:52 PM
 */

namespace Donations\Common\Interfaces;


interface IApi {

    function sendRequest($params);

    function createResponseObject($result);
}