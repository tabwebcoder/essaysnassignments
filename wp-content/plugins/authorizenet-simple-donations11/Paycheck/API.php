<?php
/**
 * Created by PhpStorm.
 * User: wasimqamar
 * Date: 26/08/17
 * Time: 8:50 PM
 */

namespace Donations\Paycheck;


use Donations\Common\Classes\PaymentResponseObject;

class API {
    private $url;
    private $errors;

    function __construct($url) {
        $this->url = $url;
    }

    function sendRequest($method, $params) {
        $client = null;
        $response = null;
        try {
            $client = new \SoapClient($this->url,
                array(
                    'trace' =>true,
                    'connection_timeout' => 500000,
                    'cache_wsdl' => WSDL_CACHE_BOTH,
                    'keep_alive' => false,
                )
            );
            $response = $client->__soapCall($method, array($params));

            $result = $response->SubmitCheckResult;
            $resultObject = simplexml_load_string($result);

            return $this->createResponseObject($resultObject);
        }
        catch (\Exception $e) {
            $messages[] = 'Unable to receive payment at the moment.</br> Reason: ' . $e->getMessage() . '.</br>';
            return new PaymentResponseObject(PaymentResponseObject::STATUS_FAILURE, $messages);
        }
    }

    function createResponseObject($result) {
        $responses = [
            'A' => PaymentResponseObject::STATUS_SUCCESS,
            'R' => PaymentResponseObject::STATUS_FAILURE,
            'I' => PaymentResponseObject::STATUS_FAILURE,
            'S' => PaymentResponseObject::STATUS_FAILURE
        ];

        $statusType = $result->type;
        $status = trim($statusType[0]);

        return new PaymentResponseObject($responses[$status], $result->message);
    }
}