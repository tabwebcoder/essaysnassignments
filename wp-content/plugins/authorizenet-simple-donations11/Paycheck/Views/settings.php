<h2>Checking Account</h2>
<table class="form-table">
    <tr valign="top">
        <th scope="row">Donation amount:</th>
        <td><input type="text" style="width:50%" name="wds_donation_paycheck_amount" value="<?php echo get_option('wds_donation_paycheck_amount'); ?>" placeholder="Paycheck donation amount" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">Merchant ID</th>
        <td><input type="text" style="width:50%" name="wds_donation_paycheck_merchant_id" value="<?php echo get_option('wds_donation_paycheck_merchant_id'); ?>" placeholder="API Login ID" /></td>
    </tr>
</table>