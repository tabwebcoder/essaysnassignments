<h2>Pay certify</h2>
<table class="form-table">
    <tr valign="top">
        <th scope="row">Donation amount:</th>
        <td><input type="text" style="width:50%" name="wds_donation_paycertify_amount" value="<?php echo get_option('wds_donation_paycertify_amount'); ?>" placeholder="Paycertify donation amount" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">API Key</th>
        <td><input type="text" style="width:50%" name="wds_donation_paycertify_api_key" value="<?php echo get_option('wds_donation_paycertify_api_key'); ?>" placeholder="API Login ID" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">API Secret</th>
        <td><input type="text" style="width:50%" name="wds_donation_paycertify_api_secret" value="<?php echo get_option('wds_donation_paycertify_api_secret'); ?>" placeholder="API Password" /></td>
    </tr>
    <tr valign="top">
        <th scope="row">Gateway API Token</th>
        <td><input type="text" style="width:50%" name="wds_donation_paycertify_gateway_token" value="<?php echo get_option('wds_donation_paycertify_gateway_token'); ?>" placeholder="API Gateway Token" /></td>
    </tr>
</table>